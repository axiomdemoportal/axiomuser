package com.mollatech.internal.handler.certificate.source;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.communication.CASettings;
import com.mollatech.axiom.connector.communication.CertificateDetailsInternal;
import com.mollatech.axiom.connector.communication.CertificateManagerInterface;
import com.mollatech.axiom.connector.communication.ThirPartyAccountInfo;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.OrganizationDetails;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.X500NameStyle;
import org.bouncycastle.asn1.x500.style.BCStrictStyle;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;

import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;

public class AxiomCertificateManager implements CertificateManagerInterface {
 private final String COUNTRY = "2.5.4.6";
 private final String STATE = "2.5.4.8";
 private final String LOCALE = "2.5.4.7";
 private final String ORGANIZATION = "2.5.4.10";
 private final String ORGANIZATION_UNIT = "2.5.4.11";
 private final String COMMON_NAME = "2.5.4.3";

    private CASettings cSetting = null;
//    private String strPFXFilePath = null;
//    private String strPassword = null;
//    private String strAlias = null;

    public AXIOMStatus Load(CASettings caSettingObj) {
        AXIOMStatus axiomstatus = new AXIOMStatus();
        cSetting = caSettingObj;
        if (cSetting != null) {
//            strPFXFilePath = cSetting.reserve1;
//            strPassword = cSetting.reserve2;
//            strAlias = cSetting.reserve3;

            axiomstatus.iStatus = 0;
            axiomstatus.strStatus = "success";
            return axiomstatus;
        }
        return null;
    }

    public AXIOMStatus Unload() {
        AXIOMStatus status = new AXIOMStatus();
        status.iStatus = 0;
        status.strStatus = "success";
        return status;
    }

    
    
    @Override
    public CertificateDetailsInternal generateCertificate(AuthUser user, OrganizationDetails org, String serialnumber, String CSR) {
        CertificateDetailsInternal certDetailsr = new CertificateDetailsInternal();
        try {
            String certb64;
            Date d = new Date();
            CertificateDetailsInfo certDetails = new CertificateDetailsInfo();
            certDetails.setCountry(org.Country);
            certDetails.setUsername(user.getUserName());
            certDetails.setState(user.street);
            certDetails.setDesignation(user.getDesignation());
            certDetails.setEmail(user.getEmail());
            certDetails.setLocation(user.getLocation());
            certDetails.setOrganisation(org.Organisation);
            certDetails.setOrganisationUnit(org.OrganisationalUnit);
            certDetails.setPhone(user.getPhoneNo());
            certDetails.setStreet(user.getStreet());
            Security.addProvider((Provider)new BouncyCastleProvider());
            KeyPairGenerator keypair = KeyPairGenerator.getInstance("RSA", "BC");
            KeyPair kp = keypair.genKeyPair();
            X509Certificate userSelfSignedCert = AxiomCertificateManager.createX509Certificate(kp, certDetails);
            certDetailsr.certificate = certb64 = new String(Base64.encode((byte[])userSelfSignedCert.getEncoded()));
            return certDetailsr;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public AuthUser readCertificateSigningRequest(String csrPEM)
    {
        PKCS10CertificationRequest csr = convertPemToPKCS10CertificationRequest(csrPEM);

        X500Name x500Name = csr.getSubject();
        System.out.println("x500Name is: " + x500Name + "\n");
        System.out.println("COUNTRY: " + getX500Field(COUNTRY, x500Name));
        System.out.println("STATE: " + getX500Field(STATE, x500Name));
        System.out.println("LOCALE: " + getX500Field(LOCALE, x500Name));
        
        
        AuthUser au= new AuthUser();
        au.country=getX500Field(COUNTRY, x500Name);
        au.organisationUnit=getX500Field(ORGANIZATION_UNIT, x500Name);
        au.organisation=getX500Field(ORGANIZATION, x500Name);
        return null;
    }
    
//    @Override
//    public CertificateDetailsInternal generateCertificate(AuthUser user, OrganizationDetails org, String serialnumber, String CSR) {
//        CertificateDetailsInternal certDetailsr = new CertificateDetailsInternal();
//        try {
//            Security.addProvider(new BouncyCastleProvider());
//           // KeyPairGenerator pgp = KeyPairGenerator.getInstance("RSA");
//            KeyStore ks = KeyStore.getInstance("JKS");
//            FileInputStream fin = new FileInputStream(new File(cSetting.reserve1));
//            ks.load(fin, cSetting.reserve2.toCharArray());
//            PrivateKey pvK = (PrivateKey) ks.getKey(cSetting.reserve3, cSetting.reserve2.toCharArray());
//            Date startDate = new Date();
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(startDate);
//            cal.add(Calendar.DATE, cSetting.durationDays);
//            long expDateString = cal.getTime().getTime();
//            PKCS10CertificationRequest pkcsRequest = convertPemToPKCS10CertificationRequest(CSR);
//            SubjectPublicKeyInfo pkInfo = pkcsRequest.getSubjectPublicKeyInfo();
//            RSAKeyParameters rsa = (RSAKeyParameters) PublicKeyFactory.createKey(pkInfo);
//            RSAPublicKeySpec rsaSpec = new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent());
//            KeyFactory kf = KeyFactory.getInstance("RSA");
//            PublicKey rsaPub = kf.generatePublic(rsaSpec);
//            X500Name x500Name = pkcsRequest.getSubject();
//          //  String userIdentity = user.getPhoneNo().replaceAll("[-+.^:,]","");
//              String userIdentity =""+new Date().getTime();
//            BigInteger serialNumber = new BigInteger(userIdentity);      // serial number for certificate
//            PrivateKey caKey = pvK;          // private key of the certifying authority (ca) certificate
//            X509Certificate caCert = (X509Certificate) ks.getCertificate(cSetting.reserve3);   // public key certificate of the certifying authority
//                        // public/private key pair that we are creating certificate for
//            X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
//            String subjectString = "CN=" + getX500Field(COMMON_NAME, x500Name) + ",OU=" + getX500Field(ORGANIZATION_UNIT, x500Name)+",O="+getX500Field(ORGANIZATION, x500Name) + ",C="+getX500Field(COUNTRY, x500Name) + ",L=" + getX500Field(LOCALE, x500Name) + ",ST=" + getX500Field(STATE, x500Name);
//            X500Principal subjectName = new X500Principal(subjectString);
//            certGen.setSerialNumber(serialNumber);
//            certGen.setIssuerDN(caCert.getSubjectX500Principal());
//            certGen.setNotBefore(startDate);
//            certGen.setNotAfter(new Date(expDateString));
//            certGen.setSubjectDN(subjectName);
//            certGen.setPublicKey(rsaPub);
//            certGen.setSignatureAlgorithm("SHA1WithRSA");
//            certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false,
//                    new AuthorityKeyIdentifierStructure(caCert));
//            certGen.addExtension(X509Extensions.SubjectKeyIdentifier, false,
//                    new SubjectKeyIdentifier(rsaPub.getEncoded()));
//            X509Certificate cert = certGen.generate(caKey, "BC");
//            certDetailsr.certificate = new String(Base64.encode((byte[]) cert.getEncoded()));
//          
//            return certDetailsr;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//    
    
      public CertificateDetailsInternal GenerateCertificateFaceforSSLandApplication(String serialno,String csr)
      {
         CertificateDetailsInternal certDetailsr = new CertificateDetailsInternal();
        try {
            Security.addProvider(new BouncyCastleProvider());
           // KeyPairGenerator pgp = KeyPairGenerator.getInstance("RSA");
            KeyStore ks = KeyStore.getInstance("JKS");
            FileInputStream fin = new FileInputStream(new File(cSetting.reserve1));
            ks.load(fin, cSetting.reserve2.toCharArray());
            PrivateKey pvK = (PrivateKey) ks.getKey(cSetting.reserve3, cSetting.reserve2.toCharArray());
            Date startDate = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.DATE, cSetting.durationDays);
            long expDateString = cal.getTime().getTime();
            PKCS10CertificationRequest pkcsRequest = convertPemToPKCS10CertificationRequest(csr);
            SubjectPublicKeyInfo pkInfo = pkcsRequest.getSubjectPublicKeyInfo();
            RSAKeyParameters rsa = (RSAKeyParameters) PublicKeyFactory.createKey(pkInfo);
            RSAPublicKeySpec rsaSpec = new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent());
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey rsaPub = kf.generatePublic(rsaSpec);
            X500Name x500Name = pkcsRequest.getSubject();
            BigInteger serialNumber = new BigInteger(serialno);      // serial number for certificate
            PrivateKey caKey = pvK;          // private key of the certifying authority (ca) certificate
            X509Certificate caCert = (X509Certificate) ks.getCertificate(cSetting.reserve3);   // public key certificate of the certifying authority
                        // public/private key pair that we are creating certificate for
            X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
            String subjectString = "CN=" + getX500Field(COMMON_NAME, x500Name) + ",OU=" + getX500Field(ORGANIZATION_UNIT, x500Name)+",O="+getX500Field(ORGANIZATION, x500Name) + ",C="+getX500Field(COUNTRY, x500Name) + ",L=" + getX500Field(LOCALE, x500Name) + ",ST=" + getX500Field(LOCALE, x500Name)+",EMAILADDRESS="+getx500FieldEmailId(x500Name);
           
            
            
            X500Principal subjectName = new X500Principal(subjectString);
            certGen.setSerialNumber(serialNumber);
            certGen.setIssuerDN(caCert.getSubjectX500Principal());
            certGen.setNotBefore(startDate);
            certGen.setNotAfter(new Date(expDateString));
            certGen.setSubjectDN(subjectName);
            certGen.setPublicKey(rsaPub);
            certGen.setSignatureAlgorithm("SHA1WithRSA");
            certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false,
                    new AuthorityKeyIdentifierStructure(caCert));
            certGen.addExtension(X509Extensions.SubjectKeyIdentifier, false,
                    new SubjectKeyIdentifier(rsaPub.getEncoded()));
            X509Certificate cert = certGen.generate(caKey, "BC");
            certDetailsr.certificate = new String(Base64.encode((byte[]) cert.getEncoded()));
          
            return certDetailsr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } 
          
          
  
          
      }
 
    
      private String getx500FieldEmailId(X500Name x500Name)
      {
          try{
           RDN cn = x500Name.getRDNs(BCStyle.EmailAddress)[0];  
           return  IETFUtils.valueToString(cn.getFirst().getValue());
          }catch(Exception ex)
          {
          ex.printStackTrace();
          }
          return null;
      }
      
       private String getX500Field(String asn1ObjectIdentifier, X500Name x500Name) {
        RDN[] rdnArray = x500Name.getRDNs(new ASN1ObjectIdentifier(asn1ObjectIdentifier));
        String retVal = null;
        for (RDN item : rdnArray) {
            retVal = item.getFirst().getValue().toString();
        }
        return retVal;
    }
    

    public static X509Certificate createX509Certificate(KeyPair keyPair, CertificateDetailsInfo certDetails) throws IOException, OperatorCreationException, CertificateException {
        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();
        if (!(publicKey instanceof RSAPublicKey) || !(privateKey instanceof RSAPrivateKey)) {
            return null;
        }
        RSAPublicKey rsaPublicKey = (RSAPublicKey)publicKey;
        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey)privateKey;
        ASN1InputStream asn1InputStream = null;
        X509Certificate x509Certificate = null;
        try {
            X500Name subjectName;
            asn1InputStream = new ASN1InputStream((InputStream)new ByteArrayInputStream(rsaPublicKey.getEncoded()));
            SubjectPublicKeyInfo pubKey = new SubjectPublicKeyInfo((ASN1Sequence)asn1InputStream.readObject());
            X500NameBuilder nameBuilder = new X500NameBuilder((X500NameStyle)new BCStrictStyle());
            nameBuilder.addRDN(BCStyle.CN, certDetails.getUsername());
            nameBuilder.addRDN(BCStyle.COUNTRY_OF_RESIDENCE, certDetails.getCountry());
            nameBuilder.addRDN(BCStyle.STREET, certDetails.getStreet());
            nameBuilder.addRDN(BCStyle.ST, certDetails.getState());
            nameBuilder.addRDN(BCStyle.L, certDetails.getLocation());
            nameBuilder.addRDN(BCStyle.O, certDetails.getOrganisation());
            nameBuilder.addRDN(BCStyle.OU, certDetails.getOrganisationUnit());
            X500Name issuerName = subjectName = nameBuilder.build();
            X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(issuerName, BigInteger.valueOf(new SecureRandom().nextInt()), GregorianCalendar.getInstance().getTime(), new Date(), subjectName, pubKey);
            AlgorithmIdentifier sigAlgId = new DefaultSignatureAlgorithmIdentifierFinder().find("SHA1withRSA");
            AlgorithmIdentifier digAlgId = new DefaultDigestAlgorithmIdentifierFinder().find(sigAlgId);
            BcRSAContentSignerBuilder sigGen = new BcRSAContentSignerBuilder(sigAlgId, digAlgId);
            RSAKeyParameters keyParams = new RSAKeyParameters(true, rsaPrivateKey.getPrivateExponent(), rsaPrivateKey.getModulus());
            ContentSigner contentSigner = sigGen.build((AsymmetricKeyParameter)keyParams);
            X509CertificateHolder certificateHolder = certBuilder.build(contentSigner);
            JcaX509CertificateConverter certConverter = new JcaX509CertificateConverter();
            x509Certificate = certConverter.getCertificate(certificateHolder);
        }
        finally {
            if (asn1InputStream != null) {
                try {
                    asn1InputStream.close();
                }
                catch (IOException e) {
                    System.out.println("E" + e.getMessage());
                }
            }
        }
        return x509Certificate;
    }
    
    
     private static PKCS10CertificationRequest convertPemToPKCS10CertificationRequest(String pem) {
       //  pem="-----BEGIN CERTIFICATE REQUEST-----"+"-----END CERTIFICATE REQUEST-----";
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        PKCS10CertificationRequest csr = null;
        ByteArrayInputStream pemStream = null;
        try {
            pemStream = new ByteArrayInputStream(pem.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            //LOG.error("UnsupportedEncodingException, convertPemToPublicKey", ex);
        }

        Reader pemReader = new BufferedReader(new InputStreamReader(pemStream));
        PEMParser pemParser = new PEMParser(pemReader);

        try {
            Object parsedObj = pemParser.readObject();

            System.out.println("PemParser returned: " + parsedObj);

            if (parsedObj instanceof PKCS10CertificationRequest) {
                csr = (PKCS10CertificationRequest) parsedObj;

            }
        } catch (IOException ex) {
        //    LOG.error("IOException, convertPemToPublicKey", ex);
        }

        return csr;
    }
    
    

//    private PublicKey getPublicKeyfromCSR(CertificationRequest csr) {
//
//    try {
//        SubjectPublicKeyInfo pubKeyInfo = csr.getCertificationRequestInfo().getSubjectPublicKeyInfo();
//        RSAKeyParameters keyParams = (RSAKeyParameters) PublicKeyFactory.createKey(pubKeyInfo);
//        KeySpec keySpec = new RSAPublicKeySpec(keyParams.getModulus(), keyParams.getExponent());
//        try {
//            KeyFactory kf = KeyFactory.getInstance("RSA");
//            return kf.generatePublic(keySpec);
//        } catch (Exception e) {
//            throw new IOException(e);
//        }
//    } catch (IOException ex) {
//            Logger.getLogger(AxiomCertificateManager.class.getName()).log(Level.SEVERE, null, ex);
//    }
//        return null;
//}
    public AXIOMStatus revokeCertificate(AuthUser user,String serialnumber, String reason) {
        AXIOMStatus status = new AXIOMStatus();
        status.iStatus = 0;
        status.strStatus = "success";
        return status;
    }

    public AXIOMStatus renewCertificate(AuthUser user,String certificate,String password,String serialnumber, String CSR) {
        AXIOMStatus status = new AXIOMStatus();
        status.iStatus = 0;
        status.strStatus = "success";
        return status;
    }

//    public AXIOMStatus GetOCSPResponse(String serilanumber) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
//
//    public AXIOMStatus GetCRL() {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }

    public AXIOMStatus GetTimeStamp() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

   
     public AXIOMStatus GetOCSPResponse(String serilanumber) {
        int result = -1;
        AXIOMStatus aStatus = new AXIOMStatus();
        aStatus.iStatus = result;
        aStatus.strStatus = "FAILED";
        try {
            URL url = new URL(cSetting.CRL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // System.out.println(conn.getResponseCode());
            int code = conn.getResponseCode();
            if (code == 200) {
                result = 0;
                aStatus.iStatus = result;
                aStatus.strStatus = "SUCCESS";
                return aStatus;
            } else {
                return aStatus;
            }
        } catch (Exception ex) {
            //Logger.getLogger(SMSHandlerTwilio.class.getName()).log(Level.SEVERE, null, ex);
            aStatus.iStatus = -2;
            aStatus.strStatus = "EXCEPTION::"+ex.getMessage();
            return aStatus;
        }
    }

    public String[] GetCRL() {
        String[] crlSrNos = new String[2];
        crlSrNos[0]= "1234567890";
        crlSrNos[1]= "7894561230";
        return crlSrNos;
    }
       @Override
    public AXIOMStatus GetServiceStatus() {
        int result = -1;
        AXIOMStatus aStatus = new AXIOMStatus();
        aStatus.iStatus = result;
        aStatus.strStatus = "FAILED";
        try {
            URL url = new URL(cSetting.CRL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // System.out.println(conn.getResponseCode());
            int code = conn.getResponseCode();
            if (code == 200) {
                result = 0;
                aStatus.iStatus = result;
                aStatus.strStatus = "SUCCESS";
                return aStatus;
            } else {
                return aStatus;
            }
        } catch (Exception ex) {
            //Logger.getLogger(SMSHandlerTwilio.class.getName()).log(Level.SEVERE, null, ex);
            aStatus.iStatus = -2;
            aStatus.strStatus = "EXCEPTION::"+ex.getMessage();
            return aStatus;
        }
    }

    @Override
    public String migrateCertificate(AuthUser user, OrganizationDetails org, String serialnumber, String CSR, String expiryDate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  

    @Override
    public CertificateDetailsInternal GenerateThirdPartyCertificates(ThirPartyAccountInfo acccountInfo, OrganizationDetails org, AuthUser auser, String csr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
        
        
        
        
    
    
    
    
    
}
