/*
 * Decompiled with CFR 0_114.
 */
package com.mollatech.internal.handler.certificate.source;

public class CertificateDetailsInfo {
    private String Country;
    private String location;
    private String username;
    private String phone;
    private String email;
    private String organisation;
    private String organisationUnit;
    private String street;
    private String designation;
    private String state;

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

    public String getLocation() {
        return this.location;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public void setOrganisationUnit(String organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCountry() {
        return this.Country;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getEmail() {
        return this.email;
    }

    public String getOrganisation() {
        return this.organisation;
    }

    public String getOrganisationUnit() {
        return this.organisationUnit;
    }

    public String getStreet() {
        return this.street;
    }

    public String getDesignation() {
        return this.designation;
    }
}
