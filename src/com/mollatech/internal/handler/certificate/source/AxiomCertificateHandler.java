/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.internal.handler.certificate.source;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.communication.CASettings;
import com.mollatech.axiom.connector.communication.CertificateDetailsInternal;
import com.mollatech.axiom.connector.communication.CertificateManagerInterface;
import com.mollatech.axiom.connector.communication.ThirPartyAccountInfo;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.OrganizationDetails;
import com.mollatech.internal.handler.geolocation.AllTrustManager;
import com.mollatech.internal.handler.geolocation.AllVerifier;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.me.enroll.Enroll;

/**
 *
 * @author Ideasventure
 */
public class AxiomCertificateHandler implements CertificateManagerInterface {

    public AxiomCertificateHandler() {

        SSLContext sslContext = null;
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
            try {
                sslContext = SSLContext.getInstance("TLS");
            } catch (NoSuchAlgorithmException ex) {
                //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
            sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

        } catch (KeyManagementException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    private CASettings cSetting = null;

    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        // TODO code application logic here
    }

    public AXIOMStatus Load(CASettings caSettingObj) {
        AXIOMStatus axiomstatus = new AXIOMStatus();
        cSetting = caSettingObj;
        if (cSetting != null) {

            axiomstatus.iStatus = 0;
            axiomstatus.strStatus = "success";
            return axiomstatus;
        }
        return null;
    }

    public AXIOMStatus Unload() {
        AXIOMStatus status = new AXIOMStatus();
        status.iStatus = 0;
        status.strStatus = "success";
        return status;
    }

    @Override
    public CertificateDetailsInternal generateCertificate(AuthUser user, OrganizationDetails org, String serialnumber, String CSR) {
        Enroll e = new Enroll();
CertificateDetailsInternal certDetail=null;
        try {

            String certificate = enroll(user.userId, CSR, user.userName, serialnumber); 
         certDetail.certificate=certificate;
            return certDetail ;

        } catch (Exception ex) {
            ex.printStackTrace();

        }

        return null;
    }

    @Override
    public AXIOMStatus revokeCertificate(AuthUser user, String serialnumber, String reason) {
        AXIOMStatus status = new AXIOMStatus();

        try {
            List<String> response = revoke(user.userId);

            if (response != null && response.size() != 0) {
                if (response.get(0).equalsIgnoreCase("000") || response.get(1).equalsIgnoreCase("000")) {
                    status.iStatus = 0;
                    status.strStatus = response.get(1);
                } else {
                    status.iStatus = -1;
                    status.strStatus = "failure";
                }
                return status;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            status.iStatus = -1;
            status.strStatus = "failure";
            return status;

        }
        status.iStatus = -1;
        status.strStatus = "failure";
        return status;
    }

    @Override
    public AXIOMStatus renewCertificate(AuthUser user, String certificate, String password, String serialnumber, String CSR) {
        AXIOMStatus status = new AXIOMStatus();

        try {

            String cert = renewal(user.getUserId(), certificate, password, serialnumber);
            if (cert != null) {
                status.iStatus = 0;
                status.strStatus = cert;
                return status;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            status.iStatus = -1;
            status.strStatus = "failure";
            return status;

        }
        status.iStatus = -1;
        status.strStatus = "failure";
        return status;
    }

//    @Override
//    public AXIOMStatus GetCRL() {
//        AXIOMStatus status = new AXIOMStatus();
//        tgCRL crl = new tgCRL();
//        tgCRL.main(null);        
//        status.iStatus = 0;
//        status.strStatus = "success";
//        return status;
//    }
//    @Override
//    public AXIOMStatus GetServiceStatus() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    @Override
    public AXIOMStatus GetTimeStamp() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AXIOMStatus GetOCSPResponse(String serilanumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetCRL() {

//        tgCRL tcCrl = new tgCRL();
//        crl.
//        
//        AXIOMStatus status = new AXIOMStatus();
//        tgCRL crl = new tgCRL();
//        tgCRL.main(null);        
//        status.iStatus = 0;
//        status.strStatus = "success";
        String[] crlSrNos = new String[2];
        crlSrNos[0] = "1234567890";
        crlSrNos[1] = "7894561230";
        return crlSrNos;
    }

    @Override
    public AXIOMStatus GetServiceStatus() {
        int result = -1;
        AXIOMStatus aStatus = new AXIOMStatus();
        aStatus.iStatus = result;
        aStatus.strStatus = "FAILED";
        try {
            URL url = new URL(cSetting.CRL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // System.out.println(conn.getResponseCode());
            int code = conn.getResponseCode();
            if (code == 200) {
                result = 0;
                aStatus.iStatus = result;
                aStatus.strStatus = "SUCCESS";
                return aStatus;
            } else {
                return aStatus;
            }
        } catch (Exception ex) {
            //Logger.getLogger(SMSHandlerTwilio.class.getName()).log(Level.SEVERE, null, ex);
            aStatus.iStatus = -2;
            aStatus.strStatus = "EXCEPTION::" + ex.getMessage();
            return aStatus;
        }
    }

    private static String enroll(java.lang.String uid, java.lang.String csr, java.lang.String uname, java.lang.String sn) {
        com.rss.RSSService service = new com.rss.RSSService();
        com.rss.RSS port = service.getRSSPort();
        return port.enroll(uid, csr, uname, sn);
    }

    private static String renewal(java.lang.String userID, java.lang.String cert, java.lang.String pass, java.lang.String other) {
        com.rss.RSSService service = new com.rss.RSSService();
        com.rss.RSS port = service.getRSSPort();
        return port.renewal(userID, cert, pass, other);
    }

    private static java.util.List<java.lang.String> revoke(java.lang.String uid) {
        com.rss.RSSService service = new com.rss.RSSService();
        com.rss.RSS port = service.getRSSPort();
        return port.revoke(uid);
    }

//    private CASettings cSetting = null;
//
//    @Override
//    public AXIOMStatus Load(CASettings caSettingObj) {
//        AXIOMStatus axiomstatus = new AXIOMStatus();
//        cSetting = caSettingObj;
//        if (cSetting != null) {
//            axiomstatus.iStatus = 0;
//            axiomstatus.strStatus = "success";
//            return axiomstatus;
//        }
//        return null;
//    }
//
//    @Override
//    public AXIOMStatus Unload() {
//        AXIOMStatus status = new AXIOMStatus();
//        status.iStatus = 0;
//        status.strStatus = "success";
//        return status;
//    }
//
//    @Override
//    public String generateCertificate(AuthUser user, OrganizationDetails org, String serialnumber, String CSR) {
//
//        Enroll e = new Enroll();
//
//        try {
//            int iResult = e.Enroll(user.userId, CSR, user.userName, serialnumber);
//            if (iResult == 0) {
//                return e.getCertificate();
//            } else {
//                System.out.println(e);
//            } 
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            e.getErrorMsg();            
//        }
//
//        return null;
//
//    }
//
//    @Override
//    public AXIOMStatus revokeCertificate(String serialnumber, String reason) {
//        AXIOMStatus status = new AXIOMStatus();
//        Revoke r = new Revoke();
//
//        try {
//            int iResult = r.Revoke(serialnumber);
//
//            if (iResult == 0) {
//                status.iStatus = 0;
//                status.strStatus = "success";
//                return status;
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            status.iStatus = -1;
//            status.strStatus = "failure";
//            return status;
//
//        }
//        status.iStatus = -1;
//        status.strStatus = "failure";
//        return status;
//    }
//
//    @Override
//    public AXIOMStatus renewCertificate(String serialnumber, String CSR) {
//        AXIOMStatus status = new AXIOMStatus();
//        Enroll e = new Enroll();
//        String cert = e.getCertificate();
//        Renew r = new Renew();
//
//        try {
//
//            int iResult = r.Renew(serialnumber, cert, CSR, null);
//            if (iResult == 0) {
//                status.iStatus = 0;
//                status.strStatus = "success";
//                return status;
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            status.iStatus = -1;
//            status.strStatus = "failure";
//            return status;
//
//        }
//        status.iStatus = -1;
//        status.strStatus = "failure";
//        return status;
//    }
//
////    @Override
////    public AXIOMStatus GetCRL() {
////        AXIOMStatus status = new AXIOMStatus();
////        tgCRL crl = new tgCRL();
////        tgCRL.main(null);        
////        status.iStatus = 0;
////        status.strStatus = "success";
////        return status;
////    }
//
////    @Override
////    public AXIOMStatus GetServiceStatus() {
////        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
////    }
//
//    @Override
//    public AXIOMStatus GetTimeStamp() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public AXIOMStatus GetOCSPResponse(String serilanumber) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    
//    @Override
//    public String[] GetCRL() {
//         
////        tgCRL tcCrl = new tgCRL();
////        crl.
////        
////        AXIOMStatus status = new AXIOMStatus();
////        tgCRL crl = new tgCRL();
////        tgCRL.main(null);        
////        status.iStatus = 0;
////        status.strStatus = "success";
//        String[] crlSrNos = new String[2];
//        crlSrNos[0]= "1234567890";
//        crlSrNos[1]= "7894561230";
//        return crlSrNos;
//    }
//
//    @Override
//    public AXIOMStatus GetServiceStatus() {
//        int result = -1;
//        AXIOMStatus aStatus = new AXIOMStatus();
//        aStatus.iStatus = result;
//        aStatus.strStatus = "FAILED";
//        try {
//            URL url = new URL(cSetting.CRL);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            // System.out.println(conn.getResponseCode());
//            int code = conn.getResponseCode();
//            if (code == 200) {
//                result = 0;
//                aStatus.iStatus = result;
//                aStatus.strStatus = "SUCCESS";
//                return aStatus;
//            } else {
//                return aStatus;
//            }
//        } catch (Exception ex) {
//            //Logger.getLogger(SMSHandlerTwilio.class.getName()).log(Level.SEVERE, null, ex);
//            aStatus.iStatus = -2;
//            aStatus.strStatus = "EXCEPTION::"+ex.getMessage();
//            return aStatus;
//        }
//    }

    @Override
    public String migrateCertificate(AuthUser user, OrganizationDetails org, String serialnumber, String CSR, String expiryDate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CertificateDetailsInternal GenerateCertificateFaceforSSLandApplication(String serialno, String csr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CertificateDetailsInternal GenerateThirdPartyCertificates(ThirPartyAccountInfo accountInfo,OrganizationDetails org,AuthUser auser,String csr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
}
