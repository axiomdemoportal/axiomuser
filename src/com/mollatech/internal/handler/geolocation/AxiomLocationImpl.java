package com.mollatech.internal.handler.geolocation;

import com.mollatech.axiom.connector.mobiletrust.AxiomLocationInterface;
import com.mollatech.axiom.connector.mobiletrust.Location;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class AxiomLocationImpl implements AxiomLocationInterface {
    
    @Override
    public Location getLocationByIp(String ipAddress) {
        try {
            //String ip = URLEncoder.encode(ipAddress, "UTF-8");

            String url = "http://ipinfo.io/" + ipAddress + "/json";
//            String[][] headers = new String[1][1];
 //String[][] headers = new String[1][2];
        
           //headers[0][0] = "User-Agent";
           //headers[0][1] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36";
//            String[][] nameValue = new String[2][2];
//            nameValue[0][0] = "latlng";
//            nameValue[0][1] = latitude + "," + longitude;
//            nameValue[1][0] = "sensor";
//            nameValue[1][1] = "false";
//String area = null;
            String sublocality = null;
            // String policality = null;
            String city = null;
            // String cityS = null;
            String state = null;
            String country = null;
            //  String zipcode = null;
            String[] response = SendDataInner(url, null, null, true);
            if (response == null && response.length <= 0) {
                return null;
            }
            if (response[1].startsWith("application/json") == true ) {

                JSONObject jsonResponse = new JSONObject(response[0]);

                city = jsonResponse.getString("city");
                state = jsonResponse.getString("region");
                country = jsonResponse.getString("country");

                Location loc = new Location();

                loc.city = city;
                loc.state = state;
                loc.country = country;
                String location = jsonResponse.getString("loc");
                String[] locationList = location.split(",");
                loc.lattitude = locationList[0];
                loc.longitude = locationList[1];
                loc.short_name = country;
                
                //loc.zipcode = zipcode;
                //System.out.println("SubLocality  :" + sublocality);
                //System.out.println("CITY  :" + city);
                //System.out.println("state  :" + state);
                //System.out.println("country  :" + country);
                //System.out.println("pincode  :" + zipcode);

                return loc;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Location getLocation(String latitude, String longitude) {
        try {

            String latlang = URLEncoder.encode(latitude + "," + longitude, "UTF-8");
            String sensor = URLEncoder.encode("false", "UTF-8");

            String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang + "&sensor=" + sensor;
            String[] response = SendDataInner(url, null, null, true);
            if (response == null && response.length <= 0) {
                return null;
            }
            if (response[1].equals("application/json; charset=UTF-8")) {



                JSONObject jsonResponse = new JSONObject(response[0]);

                String jsonStr = jsonResponse.getString("results");
                JSONArray jsonArrayResponse = new JSONArray(jsonStr);

                //  json = new JSONObject(json1);
                String json = jsonArrayResponse.getString(0);
//            String json = "{\"address_components\":[{\"long_name\":\"12\",\"types\":[],\"short_name\":\"12\"},{\"long_name\":\"Magarpatta City\",\"types\":[\"sublocality\",\"political\"],\"short_name\":\"Magarpatta City\"},{\"long_name\":\"Hadapsar\",\"types\":[\"sublocality\",\"political\"],\"short_name\":\"Hadapsar\"},{\"long_name\":\"Pune\",\"types\":[\"locality\",\"political\"],\"short_name\":\"Pune\"},{\"long_name\":\"Pune\",\"types\":[\"administrative_area_level_2\",\"political\"],\"short_name\":\"Pune\"},{\"long_name\":\"Maharashtra\",\"types\":[\"administrative_area_level_1\",\"political\"],\"short_name\":\"MH\"},{\"long_name\":\"India\",\"types\":[\"country\",\"political\"],\"short_name\":\"IN\"},{\"long_name\":\"411028\",\"types\":[\"postal_code\"],\"short_name\":\"411028\"}],\"formatted_address\":\"12, Magarpatta City, Hadapsar, Pune, Maharashtra 411028, India\",\"types\":[\"street_address\"],\"geometry\":{\"viewport\":{\"southwest\":{\"lng\":73.92945901970849,\"lat\":18.5216760197085},\"northeast\":{\"lng\":73.9321569802915,\"lat\":18.5243739802915}},\"location_type\":\"ROOFTOP\",\"location\":{\"lng\":73.930808,\"lat\":18.523025}}}";
                JSONObject jsonFirst = new JSONObject(json);
                JSONArray jArray = jsonFirst.getJSONArray("address_components");
                // JSONObject jaa = jsa.getJSONObject(0);
                String area = null;
                String sublocality = null;
                String policality = null;
                String city = null;
                String cityS = null;
                String state = null;
                String country = null;
                String zipcode = null;
                String short_name = null;
                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject jsonObj = jArray.getJSONObject(i);
                    JSONArray jsonAinner = jsonObj.getJSONArray("types");
                    for (int j = 0; j < jsonAinner.length(); j++) {
                        String jInnerArray = jsonAinner.getString(j);

                        if (jInnerArray.equalsIgnoreCase("locality")) {
                            cityS = jsonObj.getString("long_name");
                        } else if (jInnerArray.equalsIgnoreCase("sublocality")) {
                            sublocality = sublocality + jsonObj.getString("long_name");
                        } else if (jInnerArray.equalsIgnoreCase("administrative_area_level_2")) {
                            city = jsonObj.getString("long_name");
                        } else if (jInnerArray.equalsIgnoreCase("administrative_area_level_1")) {
                            state = jsonObj.getString("long_name");
                        } else if (jInnerArray.equalsIgnoreCase("country")) {
                            country = jsonObj.getString("long_name");
                        } else if (jInnerArray.equalsIgnoreCase("postal_code")) {
                            zipcode = jsonObj.getString("long_name");
                        }  else if (jInnerArray.equalsIgnoreCase("country")) {
                            short_name = jsonObj.getString("short_name");
                        }
                        
                    }
                }

                if (city == null) {
                    city = cityS;
                }
                Location loc = new Location();
                loc.area = sublocality;
                loc.city = city;
                loc.state = state;
                loc.country = country;
                loc.zipcode = zipcode;  
                loc.short_name = short_name;
                return loc;
            } else {
                return null;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    protected static String[] SendDataInner(String link, String[][] headers, String[][] NameValue, boolean bSecure) throws Exception {

        SSLContext sslContext;
        String[] returnStrs = null;
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        } catch (Exception e) {
            throw e;
        }

        URL url = new URL(link);
        String dataToSend = "";
        if (NameValue != null) {
            for (int i = 0; i < NameValue.length; i++) {
                String[] pair = NameValue[i];
                if (i == 0) {
                    dataToSend = URLEncoder.encode(pair[0], "UTF-8") + "=" + URLEncoder.encode(pair[1], "UTF-8");
                } else {
                    dataToSend = dataToSend + "&" + URLEncoder.encode(pair[0], "UTF-8") + "=" + URLEncoder.encode(pair[1], "UTF-8");
                }
            }
        }

        String text = "";
        BufferedReader reader = null;
        // Send data 
        try {
            URLConnection conn = url.openConnection();
            if (headers != null) {
                for (int i = 0; i < headers.length; i++) {
                    String[] header = headers[i];
                    conn.setRequestProperty(header[0], header[1]);
                }
            }
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(dataToSend);
            wr.flush();

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            text = sb.toString();

            String contentType = conn.getHeaderField("Content-Type");
            //if( contentType.compareTo("application/json") == true ) {
            //}

            returnStrs = new String[2];
            returnStrs[0] = text;
            returnStrs[1] = contentType;
        } catch (Exception ex) {
        } finally {
            try {
                reader.close();
            } catch (Exception ex) {
            }
        }
        return returnStrs;
    }
}
