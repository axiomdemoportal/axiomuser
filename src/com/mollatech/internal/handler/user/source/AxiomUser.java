package com.mollatech.internal.handler.user.source;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.Answer;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.AxiomConfigProvider;
import com.mollatech.axiom.connector.user.AxiomExternalSourceInterface;
import com.mollatech.axiom.connector.user.OrganizationDetails;
import com.mollatech.axiom.connector.user.PasswordPolicyDetails;
import com.mollatech.axiom.connector.user.Question;
import com.mollatech.axiom.connector.user.QuestionAndAnswer;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.HASH;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class AxiomUser implements AxiomExternalSourceInterface {

//    private static String USERID = "userid";
//    private static String USERNAME = "username";
//    private static String PASSWORD = "password";
//    private static String PHONE = "phone";
//    private static String EMAILID = "email";
//    private static String PASSWORDSTATE = "status";
//    private static String ATTEMPTS = "attempts";
//    private static String CHANNELID = "channelid";
    public static final int ACTIVE = 1;
    public static final int BLOCKED = -1;
    public static final int DELETED = -99;
    //public static String AES = "AES";
    //public static String SHA256 = "SHA-256";

    public static String AES = "AES_ENCRYPTION";
    public static String SHA256 = "SHA256_HASH";

    public AxiomUser() {
    }
    private Connection cn;
    private PreparedStatement pst, pst1;
    private ResultSet rs;
    public static AxiomConfigProvider config = null;

    @Override
    public AXIOMStatus Unload() {
        AXIOMStatus ax = new AXIOMStatus();
        ax.iStatus = 0;
        ax.strStatus = "success";
        return ax;
    }

    @Override
    public AXIOMStatus Load(AxiomConfigProvider axiomConfigProvider) {
        AXIOMStatus ax = new AXIOMStatus();

        try {
            config = axiomConfigProvider;
            Class.forName(config.getSourceDetails().getDatabaseType());

//            cn = DriverManager.getConnection(
//                    "jdbc:mysql://" + config.getSourceDetails().getHostname() + ":" + config.getSourceDetails().getPortno() + "/" + config.getSourceDetails().getDatabaseName() + "?verifyServerCertificate=false"
//                    + "&useSSL=false"
//                    + "&requireSSL=false", config.getSourceDetails().getUserName(), config.getSourceDetails().getPassword());
//            cn.close();
            ax.iStatus = 0;
            ax.strStatus = "success";
            return ax;

        } catch (Exception ex) {
            ax.strStatus = ex.getMessage();
        }
//      catch (SQLException ex) {
//            ax.strStatus = ex.getMessage();
//        }
        ax.iStatus = -1;
        return ax;
    }

    @Override
    public AuthUser getUser(String userid) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = null;

            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", userid).ignoreCase());
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));

            AuthUser aUser = null;
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return null;
            } else {
                rUser = (Users) list.get(0);
                aUser = new AuthUser();
                aUser.userId = rUser.getUserid();
                aUser.email = rUser.getEmail();
                aUser.phoneNo = rUser.getPhone();
                aUser.userName = rUser.getUsername();
                aUser.statePassword = rUser.getStatus();
                aUser.lLastAccessOn = rUser.getLastlogindate().getTime();
                aUser.iAttempts = rUser.getAttempts();
                aUser.passwordupdatedOn = rUser.getPasswordupdatedOn();
                aUser.groupid = rUser.getGroupid();
                aUser.country = rUser.getCountry();
                aUser.street = rUser.getStreet();
                aUser.location = rUser.getLocation();
                aUser.idType = rUser.getIdType();
                aUser.userIdentity = rUser.getUserIdentity();
                aUser.organisationUnit = rUser.getOrganisationUnit();
                aUser.organisation = rUser.getOrganisation();
                aUser.designation = rUser.getDesignation();
                aUser.sso = rUser.getSsoMap();
                aUser.password = rUser.getPassword();
                aUser.status=rUser.getStatus();
                aUser.lCreatedOn=rUser.getCreatedon().getTime();

            }

            return aUser;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            sSession.close();
            suSession.close();
        }

        return null;
    }

    @Override
    public AuthUser[] getAllUsers() {

        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Users[] user = new Users[list.size()];
                AuthUser[] aUser = new AuthUser[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    user[i] = (Users) list.get(i);
                    aUser[i] = new AuthUser();
                    aUser[i].userId = user[i].getUserid();
                    aUser[i].email = user[i].getEmail();
                    aUser[i].phoneNo = user[i].getPhone();
                    aUser[i].userName = user[i].getUsername();
                    aUser[i].statePassword = user[i].getStatus();
                    aUser[i].lLastAccessOn = user[i].getLastlogindate().getTime();
                    aUser[i].iAttempts = user[i].getAttempts();
                    aUser[i].lCreatedOn = user[i].getCreatedon().getTime();
                    String password = user[i].getPassword();
                    aUser[i].setPassword(AxiomProtect.AccessData(password));
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].designation = user[i].getDesignation();
                    aUser[i].country = user[i].getCountry();
                    aUser[i].street = user[i].getStreet();
                    aUser[i].location = user[i].getLocation();
                    aUser[i].idType = user[i].getIdType();
                    aUser[i].userIdentity = user[i].getUserIdentity();
                    aUser[i].organisationUnit = user[i].getOrganisationUnit();
                    aUser[i].organisation = user[i].getOrganisation();

                }
                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            sSession.close();
            suSession.close();
        }
        //return null;

    }

    public Users[] getAllOGUsers() {

        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Users[] user = new Users[list.size()];
                AuthUser[] aUser = new AuthUser[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    user[i] = (Users) list.get(i);

                }
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            sSession.close();
            suSession.close();
        }
        //return null;

    }

    @Override
    public AuthUser verifyPassword(String userid, String userpassword) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = null;

            Criteria criteria = sSession.createCriteria(Users.class);
            //    criteria.add(Restrictions.eq("channelid",config.getChannelId()));
            criteria.add(Restrictions.eq("userid", userid).ignoreCase());
            // criteria.add(Restrictions.eq("password",password));

            AuthUser aUser = null;
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return null;
            } else {
                rUser = (Users) list.get(0);
                if (rUser.getAttempts() <= 5) {
                    String systemPassword = rUser.getPassword();

                    if (config.getPasswordPolicyDetails().isbPlain()) {
                        systemPassword = rUser.getPassword();
                    } else if (config.getPasswordPolicyDetails().isbDigest()) {
                        if (config.getPasswordPolicyDetails().getEncryptionAlgo().equals(SHA256)) {
                            userpassword = HASH.getStringFromSHA256(userpassword);
                        }
                    } else if (config.getPasswordPolicyDetails().isbEncrypt()) {
                        if (config.getPasswordPolicyDetails().getEncryptionAlgo().equals(AES)) {
                            systemPassword = AxiomProtect.AccessData(systemPassword);
                        }
                    }

                    if (userpassword.equals(systemPassword)) {
                        rUser.setAttempts(0);
                        aUser = new AuthUser();
                        aUser.userId = rUser.getUserid();
                        aUser.email = rUser.getEmail();
                        aUser.phoneNo = rUser.getPhone();
                        aUser.userName = rUser.getUsername();
                        aUser.statePassword = rUser.getStatus();
                        aUser.lLastAccessOn = rUser.getLastlogindate().getTime();
                        aUser.iAttempts = rUser.getAttempts();
                        aUser.passwordupdatedOn = rUser.getPasswordupdatedOn();
                        aUser.groupid = rUser.getGroupid();
                        aUser.country = rUser.getCountry();
                        aUser.street = rUser.getStreet();
                        aUser.location = rUser.getLocation();
                        aUser.idType = rUser.getIdType();
                        aUser.userIdentity = rUser.getUserIdentity();
                        aUser.organisationUnit = rUser.getOrganisationUnit();
                        aUser.organisation = rUser.getOrganisation();
                        aUser.designation = rUser.getDesignation();

                    } else {
                        rUser.setAttempts(rUser.getAttempts() + 1);
                    }

                } else {
                    rUser.setStatus(-1);
                }
                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.save(rUser);
                    tx.commit();

                    return aUser;

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            sSession.close();
            suSession.close();
        }

        return null;

    }

    @Override
    public int changePassword(String userid, String oldpassword, String newpassword) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = null;//new Users();
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", userid));

            String oldSecurePassword = null;
            String newSecurePassword = null;
            if (config.getPasswordPolicyDetails().isbPlain()) {
                oldSecurePassword = oldpassword;
                newSecurePassword = newpassword;
            } else if (config.getPasswordPolicyDetails().isbDigest()) {
                if (config.getPasswordPolicyDetails().getEncryptionAlgo().equals(SHA256)) {
                    oldSecurePassword = HASH.getStringFromSHA256(oldpassword);
                    newSecurePassword = HASH.getStringFromSHA256(newpassword);
                }
            } else if (config.getPasswordPolicyDetails().isbEncrypt()) {
                if (config.getPasswordPolicyDetails().getEncryptionAlgo().equals(AES)) {
                    oldSecurePassword = AxiomProtect.ProtectData(oldpassword);
                    newSecurePassword = AxiomProtect.ProtectData(newpassword);
                }
            }

            criteria.add(Restrictions.eq("password", oldSecurePassword));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return -2;
            } else {
                rUser = (Users) list.get(0);

                //String newSecurePassword = AxiomProtect.ProtectData(newpassword);
                rUser.setPassword(newSecurePassword);
                //rUser.setPassword(newpassword);

                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.save(rUser);
                    tx.commit();

                    return 0;

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            sSession.close();
            suSession.close();
        }

        return -1;
    }

//    private byte[] SHA(String message) {
//        try {
//            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
//            byte[] array = sha1.digest(message.getBytes());
//            return array;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    private String SHA1(String message) {
//        try {
//            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
//            byte[] array = sha1.digest(message.getBytes());
//            return arrayToString(array);
//        } catch (Exception e) {
//            return null;
//        }
//    }
    private String arrayToString(byte[] array) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
            sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString();
    }

//    @Override
//    public int resetPassword(String userid) {
//        try {
//
//            Date d = new Date();
//            String strPassword = SHA1(d.toString() + userid);
//            strPassword = strPassword.substring(0, 10);
//
//            String newSecurePassword = AxiomProtect.ProtectData(strPassword);
//
//            String sql = "UPDATE " + config.getSourceDetails().getTableName() + " SET " + PASSWORD + "= ? WHERE " + USERID + " =?";
//            //String sql = "UPDATE " + config.getSourceDetails().getTableName() + " SET " + PASSWORD + "= ? WHERE " + USERID + " =?" + CHANNELID + " =?";
//            pst = cn.prepareStatement(sql);
//            //pst.setString(1, strPassword);
//            pst.setString(1, newSecurePassword);
//            pst.setString(2, userid);
//            //   pst.setString(3, config.getChannelId());
//
//            int a = pst.executeUpdate();
//            if (a != 0) {
//                return 0;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return 1;
//    }
    @Override
    public int resetPassword(String userid) {

        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = new Users();
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            criteria.add(Restrictions.eq("userid", userid));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return -2;
            } else {
                rUser = (Users) list.get(0);

                Date d = new Date();
                String strPassword = HASH.getStringFromSHA1(d.toString() + userid);
                strPassword = strPassword.substring(0, 10);

                String newSecurePassword = null;
                if (config.getPasswordPolicyDetails().isbPlain()) {

                    newSecurePassword = strPassword;
                } else if (config.getPasswordPolicyDetails().isbDigest()) {
                    if (config.getPasswordPolicyDetails().getEncryptionAlgo().equals(SHA256)) {

                        newSecurePassword = HASH.getStringFromSHA256(strPassword);
                    }
                } else if (config.getPasswordPolicyDetails().isbEncrypt()) {
                    if (config.getPasswordPolicyDetails().getEncryptionAlgo().equals(AES)) {

                        newSecurePassword = AxiomProtect.ProtectData(strPassword);
                    }
                }

                rUser.setPassword(newSecurePassword);

                //rUser.setPassword(newpassword);
                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.save(rUser);
                    tx.commit();

                    return 0;

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            sSession.close();
            suSession.close();
        }

        return -1;

    }

    @Override
    public int removePassword(String userid) {

        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = new Users();
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            criteria.add(Restrictions.eq("userid", userid));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return -2;
            } else {
                rUser = (Users) list.get(0);

                rUser.setPassword(null);

                //rUser.setPassword(newpassword);
                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.save(rUser);
                    tx.commit();

                    return 0;

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            sSession.close();
            suSession.close();
        }

        return -1;

    }

//    @Override
//    public String GetPassword(String userid) {
//
//        AuthUser user = new AuthUser();
//        try {
//
//            String sql = "select " + PASSWORD + " from " + config.getSourceDetails().getTableName() + " where " + USERID + "= ?";
//
//            pst = cn.prepareStatement(sql);
//            pst.setString(1, userid);
//
//            rs = pst.executeQuery();
//            String password = null;
//
//            while (rs.next()) {
//                password = rs.getString(PASSWORD);
//
//            }
//
//            if (config.getPasswordPolicyDetails().isbDigest()) {
//            } else if (config.getPasswordPolicyDetails().isbEncrypt()) {
//            } else if (config.getPasswordPolicyDetails().isbPlain()) {
//                String securePassword = AxiomProtect.AccessData(password);
//                //return password;
//                return securePassword;
//            }
//
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//
//    }
    @Override
    public String GetPassword(String userid) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = null;

            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", userid));
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            String securePassword = null;

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return null;
            } else {
                rUser = (Users) list.get(0);

                if (config.getPasswordPolicyDetails().isbPlain()) {

                    securePassword = rUser.getPassword();
                } else if (config.getPasswordPolicyDetails().isbDigest()) {
                    if (config.getPasswordPolicyDetails().getEncryptionAlgo().equals(SHA256)) {

                        securePassword = rUser.getPassword();
                    }
                } else if (config.getPasswordPolicyDetails().isbEncrypt()) {
                    if (config.getPasswordPolicyDetails().getEncryptionAlgo().equals(AES)) {

                        securePassword = AxiomProtect.AccessData(rUser.getPassword());
                    }
                }

            }

            return securePassword;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            sSession.close();
            suSession.close();
        }
    }

    @Override
    public AuthUser[] SearchUsers(String value) {

        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        System.out.println("SearchUsers for >> "+value);
        System.out.println("SearchUsers channelid for >> "+config.getChannelId());
        try {

            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            criteria.add(Restrictions.ne("status", DELETED));

            Criterion completeCondition
                    = Restrictions.disjunction().add(Restrictions.like("username", "%" + value + "%").ignoreCase())
                    .add(Restrictions.like("email", "%" + value + "%").ignoreCase())
                    .add(Restrictions.like("phone", "%" + value + "%").ignoreCase());

            criteria.add(completeCondition);
            System.out.println("SearchUsers condition build >> "+criteria.toString());

//            //if ( type == 1 )        // name
//                criteria.add();
//            //else if ( type == 2 )  // emailid
//                criteria.add(Restrictions.like("emailid", "%" + tag + "%"));
//            //else if ( type == 3 ) //phone
//                criteria.add(Restrictions.like("phone", "%" + tag + "%"));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                System.out.println("SearchUsers result not found >> ");
                return null;                
            } else {
                System.out.println("SearchUsers result found >> ");
                Users[] user = new Users[list.size()];
                AuthUser[] aUser = new AuthUser[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    user[i] = (Users) list.get(i);
                    aUser[i] = new AuthUser();
                    aUser[i].userId = user[i].getUserid();
                    aUser[i].email = user[i].getEmail();
                    aUser[i].phoneNo = user[i].getPhone();
                    aUser[i].userName = user[i].getUsername();
                    aUser[i].statePassword = user[i].getStatus();
                    aUser[i].lLastAccessOn = user[i].getLastlogindate().getTime();
                    aUser[i].iAttempts = user[i].getAttempts();
                    aUser[i].lCreatedOn = user[i].getCreatedon().getTime();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].passwordupdatedOn = user[i].getPasswordupdatedOn();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].country = user[i].getCountry();
                    aUser[i].street = user[i].getStreet();
                    aUser[i].location = user[i].getLocation();
                    aUser[i].idType = user[i].getIdType();
                    aUser[i].userIdentity = user[i].getUserIdentity();
                    aUser[i].organisationUnit = user[i].getOrganisationUnit();
                    aUser[i].organisation = user[i].getOrganisation();
                    aUser[i].designation = user[i].getDesignation();
                    aUser[i].sso = user[i].getSsoMap();
                }
                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        } finally {
            sSession.close();
            suSession.close();
        }

    }

    @Override
    protected void finalize() throws Throwable {
        cn.close();
        pst.close();
        pst1.close();
        rs.close();
    }

    @Override
    public int AssignPassword(String userid, String password, PasswordPolicyDetails passwordpolicy) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = new Users();

            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", userid));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return -2;
            } else {
                rUser = (Users) list.get(0);

                String securePassword = null;
                if (config.getPasswordPolicyDetails().isbPlain()) {

                    securePassword = password;
                } else if (config.getPasswordPolicyDetails().isbDigest()) {
                    if (config.getPasswordPolicyDetails().getEncryptionAlgo().equals(SHA256)) {

                        securePassword = HASH.getStringFromSHA256(password);
                    }
                } else if (config.getPasswordPolicyDetails().isbEncrypt()) {
                    if (config.getPasswordPolicyDetails().getEncryptionAlgo().equals(AES)) {

                        securePassword = AxiomProtect.ProtectData(password);
                    }
                }

                rUser.setPassword(securePassword);
                //rUser.setPassword(password);

                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.save(rUser);
                    tx.commit();

                    return 0;

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            sSession.close();
            suSession.close();
        }

        return -1;
    }

    @Override
    public int getCountOfUsers() {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            criteria.setProjection(Projections.rowCount());
            Long count = (Long) criteria.uniqueResult();
            Integer i = (int) (long) count;
            return i;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return 0;
    }

    //@Override
//    public int CreateUser(String userName, String phone, String email) {
//
//        Date d = new Date();
//        String userid = HASH.getStringFromSHA1(userName + config.getChannelId() + email + phone + d.toString());
//        Users rUser = new Users();
//        rUser.setEmail(email);
//        rUser.setUserid(userid);
//        rUser.setUsername(userName);
//        rUser.setLastlogindate(new Date());
//        rUser.setCreatedon(new Date());
//        rUser.setAttempts(0);
//        rUser.setPhone(phone);
//        rUser.setStatus(1);
//        rUser.setChannelid(config.getChannelId());
//        //rUser.setPassword(null);
//
//        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
//        Session sSession = suSession.openSession();
//        Transaction tx = null;
//        try {
//            tx = sSession.beginTransaction();
//            sSession.save(rUser);
//            tx.commit();
//
//            return 0;
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        } finally {
//            //sSession.close();
//            //suSession.close();
//            sSession.close();
//            suSession.close();
//        }
//        return -2;
//    }
    @Override
    public int ChangeStatus(String userid, int iType) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();

        try {

            Users rObj = new Users();
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", userid));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return 1;
            } else {

                rObj = (Users) list.get(0);

                if (iType == ACTIVE) {
                    rObj.setAttempts(0);
                }
                rObj.setStatus(iType);
                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.save(rObj);
                    tx.commit();

                    return 0;

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            sSession.close();
            suSession.close();
        }
        return 1;

    }

    @Override
    public int EditUser(String userid, String username, String phone, String email, int groupid, String organisation, String userIdentity, String idType, String organisationUnit, String country, String location, String street, String designation) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();

        try {

            Users aObj = new Users();
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", userid));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return -2;
            } else {
                Date d = new Date();

                aObj = (Users) list.get(0);
                aObj.setUsername(username);
                aObj.setEmail(email);
                aObj.setPhone(phone);
                aObj.setLastlogindate(d);
                aObj.setGroupid(groupid);
                if (country != null) {
                    aObj.setCountry(country);
                }
                if (designation != null) {
                    aObj.setDesignation(designation);
                }
//                aObj.setLastlogindate(new Date());
                if (location != null) {
                    aObj.setLocation(location);
                }
                if (street != null) {
                    aObj.setStreet(street);
                }
                if (organisation != null) {
                    aObj.setOrganisation(organisation);
                }
                if (organisationUnit != null) {
                    aObj.setOrganisationUnit(organisationUnit);
                }
                if (userIdentity != null) {
                    aObj.setUserIdentity(userIdentity);
                }
                if (idType != null) {
                    aObj.setIdType(idType);
                }
                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.save(aObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return -5;

    }

    @Override
    public int EditUserForPush(String userid, String username, String phone, String email, int groupid, String organisation, String userIdentity, String idType, String organisationUnit, String country, String location, String street, String designation, Integer defaultTokenType) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();

        try {

            Users aObj = new Users();
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", userid));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return -2;
            } else {
                Date d = new Date();

                aObj = (Users) list.get(0);
                aObj.setUsername(username);
                aObj.setEmail(email);
                aObj.setPhone(phone);
                aObj.setLastlogindate(d);
                aObj.setGroupid(groupid);
                if (country != null) {
                    aObj.setCountry(country);
                }
                if (designation != null) {
                    aObj.setDesignation(designation);
                }
//                aObj.setLastlogindate(new Date());
                if (location != null) {
                    aObj.setLocation(location);
                }
                if (street != null) {
                    aObj.setStreet(street);
                }
                if (organisation != null) {
                    aObj.setOrganisation(organisation);
                }
                if (organisationUnit != null) {
                    aObj.setOrganisationUnit(organisationUnit);
                }
                if (userIdentity != null) {
                    aObj.setUserIdentity(userIdentity);
                }
                if (idType != null) {
                    aObj.setIdType(idType);
                }
                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.save(aObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return -5;

    }
    
    public AuthUser CheckUserUsingPhone(String phoneNumber) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = null;
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            criteria.add(Restrictions.eq("phone", phoneNumber).ignoreCase());
            criteria.add(Restrictions.ne("status", DELETED));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                rUser = (Users) list.get(0);
                AuthUser aUser = new AuthUser();
                aUser.userId = rUser.getUserid();
                aUser.email = rUser.getEmail();
                aUser.phoneNo = rUser.getPhone();
                aUser.userName = rUser.getUsername();
                aUser.statePassword = rUser.getStatus();
                aUser.lLastAccessOn = rUser.getLastlogindate().getTime();
                aUser.iAttempts = rUser.getAttempts();
                aUser.lCreatedOn = rUser.getCreatedon().getTime();
                aUser.passwordupdatedOn = rUser.getPasswordupdatedOn();
                aUser.groupid = rUser.getGroupid();
                aUser.country = rUser.getCountry();
                aUser.street = rUser.getStreet();
                aUser.location = rUser.getLocation();
                aUser.idType = rUser.getIdType();
                aUser.userIdentity = rUser.getUserIdentity();
                aUser.organisationUnit = rUser.getOrganisationUnit();
                aUser.organisation = rUser.getOrganisation();
                aUser.designation = rUser.getDesignation();

                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;

    }

    public AuthUser CheckUserUsingEmail(String emailid) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = null;

            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            criteria.add(Restrictions.eq("email", emailid));
            criteria.add(Restrictions.ne("status", DELETED));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                rUser = (Users) list.get(0);
                AuthUser aUser = new AuthUser();
                aUser.userId = rUser.getUserid();
                aUser.email = rUser.getEmail();
                aUser.phoneNo = rUser.getPhone();
                aUser.userName = rUser.getUsername();
                aUser.statePassword = rUser.getStatus();
                aUser.lLastAccessOn = rUser.getLastlogindate().getTime();
                aUser.iAttempts = rUser.getAttempts();
                aUser.lCreatedOn = rUser.getCreatedon().getTime();
                aUser.passwordupdatedOn = rUser.getPasswordupdatedOn();
                aUser.groupid = rUser.getGroupid();
                aUser.country = rUser.getCountry();
                aUser.street = rUser.getStreet();
                aUser.location = rUser.getLocation();
                aUser.idType = rUser.getIdType();
                aUser.userIdentity = rUser.getUserIdentity();
                aUser.organisationUnit = rUser.getOrganisationUnit();
                aUser.organisation = rUser.getOrganisation();
                aUser.designation = rUser.getDesignation();
                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }

    public AuthUser CheckUserUsingUserId(String userid) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = null;

            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", userid));
            criteria.add(Restrictions.ne("status", DELETED));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {

                return null;

            } else {
                rUser = (Users) list.get(0);
                AuthUser aUser = new AuthUser();
                aUser.userId = rUser.getUserid();
                aUser.email = rUser.getEmail();
                aUser.phoneNo = rUser.getPhone();
                aUser.userName = rUser.getUsername();
                aUser.statePassword = rUser.getStatus();
                aUser.lLastAccessOn = rUser.getLastlogindate().getTime();
                aUser.iAttempts = rUser.getAttempts();
                aUser.passwordupdatedOn = rUser.getPasswordupdatedOn();
                aUser.lCreatedOn = rUser.getCreatedon().getTime();
                aUser.groupid = rUser.getGroupid();
                aUser.country = rUser.getCountry();
                aUser.street = rUser.getStreet();
                aUser.location = rUser.getLocation();
                aUser.idType = rUser.getIdType();
                aUser.userIdentity = rUser.getUserIdentity();
                aUser.organisationUnit = rUser.getOrganisationUnit();
                aUser.organisation = rUser.getOrganisation();
                aUser.designation = rUser.getDesignation();
                return aUser;

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;

    }

    public boolean TestConnection() {

        try {
            String dbCategory = config.getSourceDetails().getDbCategory();
            if (dbCategory.equalsIgnoreCase("Mysql")) {
                Class.forName(config.getSourceDetails().getDatabaseType());

                if (config.getSourceDetails().getHostname().isEmpty() == true
                        || config.getSourceDetails().getDatabaseName().isEmpty() == true
                        || config.getSourceDetails().getClassName().isEmpty() == true
                        || config.getSourceDetails().getTableName().isEmpty() == true
                        || config.getSourceDetails().getUserName().isEmpty() == true
                        || config.getSourceDetails().getPassword().isEmpty() == true) {
                    return false;
                }

                cn = DriverManager.getConnection(
                        "jdbc:mysql://" + config.getSourceDetails().getHostname() + ":" + config.getSourceDetails().getPortno() + "/" + config.getSourceDetails().getDatabaseName() + "?verifyServerCertificate=false"
                        + "&useSSL=false"
                        + "&requireSSL=false", config.getSourceDetails().getUserName(), config.getSourceDetails().getPassword());
                cn.close();

                return true;
            } else if (dbCategory.equalsIgnoreCase("Oracle")) { // changed by abhishek
                Class.forName(config.getSourceDetails().getDatabaseType());

                if (config.getSourceDetails().getHostname().isEmpty() == true
                        || config.getSourceDetails().getDatabaseName().isEmpty() == true
                        || config.getSourceDetails().getClassName().isEmpty() == true
                        || config.getSourceDetails().getTableName().isEmpty() == true
                        || config.getSourceDetails().getUserName().isEmpty() == true
                        || config.getSourceDetails().getPassword().isEmpty() == true) {
                    return false;
                }

                cn = DriverManager.getConnection(
                        "jdbc:oracle:thin:@" + config.getSourceDetails().getHostname() + ":" + config.getSourceDetails().getPortno() + ":" + config.getSourceDetails().getDatabaseName(),
                        config.getSourceDetails().getUserName(), config.getSourceDetails().getPassword());
                cn.close();
                return true;
            }
            // changes end
            return false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

//    public boolean TestConnection() {
//
//        try {
//
//            Class.forName(config.getSourceDetails().getDatabaseType());
//
//            if (config.getSourceDetails().getHostname().isEmpty() == true
//                    || config.getSourceDetails().getDatabaseName().isEmpty() == true
//                    || config.getSourceDetails().getClassName().isEmpty() == true
//                    || config.getSourceDetails().getTableName().isEmpty() == true
//                    || config.getSourceDetails().getUserName().isEmpty() == true
//                    || config.getSourceDetails().getPassword().isEmpty() == true) {
//                return false;
//            }
//
//            cn = DriverManager.getConnection(
//                    "jdbc:mysql://" + config.getSourceDetails().getHostname() + ":" + config.getSourceDetails().getPortno() + "/" + config.getSourceDetails().getDatabaseName() + "?verifyServerCertificate=false"
//                    + "&useSSL=false"
//                    + "&requireSSL=false", config.getSourceDetails().getUserName(), config.getSourceDetails().getPassword());
//            cn.close();
//
//            return true;
//
////            SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
////            Session sSession = suSession.openSession();
////
//////            if (sSession != null) {
//////                sSession.close();
//////                suSession.close();
//////                return true;
//////            }
////
////            boolean available = false;
////            available = sSession.isConnected();
////            sSession.close();
////            suSession.close();
////            return available;
////            Users rUser = null;
////
////            Criteria criteria = sSession.createCriteria(Users.class);
////            List list = criteria.list();
////            if (list == null || list.isEmpty() == true) {
////                sSession.close();
////                suSession.close();
////                return false;
////            } else {
////                sSession.close();
////                suSession.close();
////                return true;
////            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//
//    }
    public int DeleteUser(String userid) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rObj = new Users();
            Users objectToInsert = new Users();
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", userid));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return 1;
            } else {
                objectToInsert = (Users) list.get(0);
                rObj = (Users) list.get(0);
                Transaction tx = null;
                try {
                    String deleteuser = LoadSettings.g_sSettings.getProperty("user.delete");
                    if (deleteuser.equals("true")) {
                        String DeletedStatus = LoadSettings.g_sSettings.getProperty("user.deletedflag.status");
                       
                        if (DeletedStatus.equals("true")) {
                            tx = sSession.beginTransaction();
                            sSession.delete(rObj);
                            tx.commit();
                            objectToInsert.setUserid("Deleted:" + userid);
                            tx = sSession.beginTransaction();
                            sSession.save(objectToInsert);
                            tx.commit();
                            return 0;
                        } else {
                            tx = sSession.beginTransaction();
                            sSession.delete(rObj);
                            tx.commit();
                            return 0;

                        }
                    } else {
                        tx = sSession.beginTransaction();
                        rObj.setStatus(DELETED);
                        sSession.save(rObj);
                        tx.commit();
                        return 0;
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            sSession.close();
            suSession.close();
        }
        return 1;

    }

    public Question[] GetValidationQuestions(String userid) {
        Question[] question = new Question[3];
        question[0] = new Question();
        question[0].setQuestion("What is your mather's name?");
        question[1] = new Question();
        question[1].setQuestion("What is your credit limit?");
        question[2] = new Question();
        question[2].setQuestion("What is your phone number?");
        return question;
    }

    public int ValidateUserDetails(String userid, Answer[] ans) {

        if (ans == null) {
            return -1;
        }

        boolean bAns1 = false, bAns2 = false, bAns3 = false;

        for (int i = 0; i < 3; i++) {
            Question q = ans[i].getQuestion();
            if (q == null) {
                return -2;
            }
            String strQuestion = q.getQuestion();
            if (strQuestion == null) {
                return -3;
            }
            String answer = ans[i].getAnswer();
            if (answer == null) {
                return -4;
            }
            if (strQuestion.compareTo("What is your mather's name?") == 0) {
                if (answer.compareTo("julie") == 0) {
                    bAns1 = true;
                }
            } else if (strQuestion.compareTo("What is your credit limit?") == 0) {
                if (answer.compareTo("5000") == 0) {
                    bAns1 = true;
                }
            } else if (strQuestion.compareTo("What is your phone number?") == 0) {
                if (answer.compareTo("1234567890") == 0) {
                    bAns1 = true;
                }
            }
        }

        if (bAns1 == true && bAns2 == true && bAns3 == true) {
            return 0;
        }
        return -9;

    }

    public OrganizationDetails GetOrganizationDetails(String userid) {

        AuthUser authUser = this.getUser(userid);
        OrganizationDetails orgDetailsObj = null;
        if (authUser != null) {
            orgDetailsObj = new OrganizationDetails();
            orgDetailsObj.City = authUser.getLocation();
            orgDetailsObj.Country = authUser.getCountry();
            orgDetailsObj.Organisation = authUser.getOrganisation();
            orgDetailsObj.OrganisationalUnit = authUser.getOrganisationUnit();
            orgDetailsObj.State = authUser.getLocation();
            orgDetailsObj.userId = userid;
            orgDetailsObj.pincode = "50480";
        }
        return orgDetailsObj;

    }

    public AuthUser CheckUserUsingName(String username) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = null;

            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            criteria.add(Restrictions.eq("username", username));
            criteria.add(Restrictions.ne("status", DELETED));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                rUser = (Users) list.get(0);
                AuthUser aUser = new AuthUser();
                aUser.userId = rUser.getUserid();
                aUser.email = rUser.getEmail();
                aUser.phoneNo = rUser.getPhone();
                aUser.userName = rUser.getUsername();
                aUser.statePassword = rUser.getStatus();
                aUser.lLastAccessOn = rUser.getLastlogindate().getTime();
                aUser.iAttempts = rUser.getAttempts();
                aUser.lCreatedOn = rUser.getCreatedon().getTime();
                aUser.passwordupdatedOn = rUser.getPasswordupdatedOn();
                aUser.groupid = rUser.getGroupid();
                aUser.country = rUser.getCountry();
                aUser.street = rUser.getStreet();
                aUser.location = rUser.getLocation();
                aUser.idType = rUser.getIdType();
                aUser.userIdentity = rUser.getUserIdentity();
                aUser.organisationUnit = rUser.getOrganisationUnit();
                aUser.organisation = rUser.getOrganisation();
                aUser.designation = rUser.getDesignation();

                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }

    public AuthUser[] getAllUserByStatus(int limit) {

        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()).ignoreCase());
            criteria.addOrder(Order.desc("createdon"));
            //    criteria.add(Restrictions.eq("status", istatus));
            criteria.setMaxResults(limit);
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Users[] user = new Users[list.size()];
                AuthUser[] aUser = new AuthUser[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    user[i] = (Users) list.get(i);
                    aUser[i] = new AuthUser();
                    aUser[i].userId = user[i].getUserid();
                    aUser[i].email = user[i].getEmail();
                    aUser[i].phoneNo = user[i].getPhone();
                    aUser[i].userName = user[i].getUsername();
                    aUser[i].statePassword = user[i].getStatus();
                    aUser[i].lLastAccessOn = user[i].getLastlogindate().getTime();
                    aUser[i].iAttempts = user[i].getAttempts();
                    aUser[i].lCreatedOn = user[i].getCreatedon().getTime();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].passwordupdatedOn = user[i].getPasswordupdatedOn();

                    aUser[i].country = user[i].getCountry();
                    aUser[i].street = user[i].getStreet();
                    aUser[i].location = user[i].getLocation();
                    aUser[i].idType = user[i].getIdType();
                    aUser[i].userIdentity = user[i].getUserIdentity();
                    aUser[i].organisationUnit = user[i].getOrganisationUnit();
                    aUser[i].organisation = user[i].getOrganisation();
                    aUser[i].designation = user[i].getDesignation();
                }
                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }

//    public AuthUser[] SearchUsersByStatus(String value, int istatus) {
//
//
//        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
//        Session sSession = suSession.openSession();
//        Criteria criteria = sSession.createCriteria(Users.class);
//        criteria.add(Restrictions.eq("channelid", config.getChannelId()));
//        try {
//            if (istatus != -99) {
//                //  criteria.add(Restrictions.eq("channelid",config.getChannelId()));
//                criteria.add(Restrictions.eq("status", istatus));
//
//
//                Criterion completeCondition =
//                        Restrictions.disjunction()
//                        .add(Restrictions.like("username", "%" + value + "%"))
//                        .add(Restrictions.like("email", "%" + value + "%"))
//                        .add(Restrictions.like("phone", "%" + value + "%"));
//
//
//                criteria.add(completeCondition);
//
//                List list = criteria.list();
//                if (list == null || list.isEmpty() == true) {
//                    return null;
//                } else {
//                    Users[] user = new Users[list.size()];
//                    AuthUser[] aUser = new AuthUser[list.size()];
//                    for (int i = 0; i < list.size(); i++) {
//                        user[i] = (Users) list.get(i);
//                        aUser[i] = new AuthUser();
//                        aUser[i].userId = user[i].getUserid();
//                        aUser[i].email = user[i].getEmail();
//                        aUser[i].phoneNo = user[i].getPhone();
//                        aUser[i].userName = user[i].getUsername();
//                        aUser[i].statePassword = user[i].getStatus();
//                        aUser[i].lLastAccessOn = user[i].getLastlogindate().getTime();
//                        aUser[i].iAttempts = user[i].getAttempts();
//                        aUser[i].lCreatedOn = user[i].getCreatedon().getTime();
//                    }
//                    return aUser;
//                }
//            } else if (istatus == 99) {
//                Criterion completeCondition =
//                        Restrictions.disjunction()
//                        .add(Restrictions.like("username", "%" + value + "%"))
//                        .add(Restrictions.like("email", "%" + value + "%"))
//                        .add(Restrictions.like("phone", "%" + value + "%"));
//
//
//                criteria.add(completeCondition);
//
//                List list = criteria.list();
//                if (list == null || list.isEmpty() == true) {
//                    return null;
//                } else {
//                    Users[] user = new Users[list.size()];
//                    AuthUser[] aUser = new AuthUser[list.size()];
//                    for (int i = 0; i < list.size(); i++) {
//                        user[i] = (Users) list.get(i);
//                        aUser[i] = new AuthUser();
//                        aUser[i].userId = user[i].getUserid();
//                        aUser[i].email = user[i].getEmail();
//                        aUser[i].phoneNo = user[i].getPhone();
//                        aUser[i].userName = user[i].getUsername();
//                        aUser[i].statePassword = user[i].getStatus();
//                        aUser[i].lLastAccessOn = user[i].getLastlogindate().getTime();
//                        aUser[i].iAttempts = user[i].getAttempts();
//                        aUsier[i].lCreatedOn = user[i].getCreatedon().getTime();
//                    }
//                    return aUser;
//                }
//
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            sSession.close();
//            suSession.close();
//        }
//        return null;
//    }
    public int getCountOfUserByStatus(int istatus, String keyword) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users rUser = new Users();

            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()).ignoreCase());
            criteria.add(Restrictions.eq("status", istatus));
            //criteria.add(Restrictions.eq("name", istatus));

            Criterion completeCondition
                    = Restrictions.disjunction()
                    .add(Restrictions.like("username", "%" + keyword + "%").ignoreCase())
                    .add(Restrictions.like("email", "%" + keyword + "%").ignoreCase())
                    .add(Restrictions.like("phone", "%" + keyword + "%").ignoreCase());

            criteria.add(completeCondition);

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {

                return 0;

            } else {
                int count = list.size();

                return count;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return 0;
    }

    public AuthUser[] SearchUsersByID(String UserID) {

        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        System.out.println("SearchUsersByID with >> "+UserID);
        System.out.println("SearchUsersByID with channelid >> "+config.getChannelId());
        try {

            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()).ignoreCase());
            criteria.add(Restrictions.eq("userid", UserID).ignoreCase());
            
//            Criterion completeCondition =
//                    Restrictions.disjunction().add(Restrictions.like("username", "%" + value + "%"))
//                    .add(Restrictions.like("email", "%" + value + "%"))
//                    .add(Restrictions.like("phone", "%" + value + "%"));
//
//            criteria.add(completeCondition);
//            //if ( type == 1 )        // name
//                criteria.add();
//            //else if ( type == 2 )  // emailid
//                criteria.add(Restrictions.like("emailid", "%" + tag + "%"));
//            //else if ( type == 3 ) //phone
//                criteria.add(Restrictions.like("phone", "%" + tag + "%"));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                System.out.println("SearchUsersByID user not found >> ");
                return null;
            } else {
                Users[] user = new Users[list.size()];
                AuthUser[] aUser = new AuthUser[list.size()];
                System.out.println("SearchUsersByID user found >> ");
                for (int i = 0; i < list.size(); i++) {
                    user[i] = (Users) list.get(i);
                    aUser[i] = new AuthUser();
                    aUser[i].userId = user[i].getUserid();
                    aUser[i].email = user[i].getEmail();
                    aUser[i].phoneNo = user[i].getPhone();
                    aUser[i].userName = user[i].getUsername();
                    aUser[i].statePassword = user[i].getStatus();
                    aUser[i].lLastAccessOn = user[i].getLastlogindate().getTime();
                    aUser[i].iAttempts = user[i].getAttempts();
                    aUser[i].lCreatedOn = user[i].getCreatedon().getTime();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].passwordupdatedOn = user[i].getPasswordupdatedOn();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].country = user[i].getCountry();
                    aUser[i].street = user[i].getStreet();
                    aUser[i].location = user[i].getLocation();
                    aUser[i].idType = user[i].getIdType();
                    aUser[i].userIdentity = user[i].getUserIdentity();
                    aUser[i].organisationUnit = user[i].getOrganisationUnit();
                    aUser[i].organisation = user[i].getOrganisation();
                    aUser[i].designation = user[i].getDesignation();
                }
                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }

    public AuthUser[] SearchUsersByStatus(String value, int istatus) {

        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        Criteria criteria = sSession.createCriteria(Users.class);
        criteria.add(Restrictions.eq("channelid", config.getChannelId()));
        try {

            if (istatus == 99) { //this meaning listing for all
            } else { //this meanings listing for specific 
                criteria.add(Restrictions.eq("status", istatus));
            }

            Criterion completeCondition
                    = Restrictions.disjunction()
                    .add(Restrictions.like("username", "%" + value + "%").ignoreCase())
                    .add(Restrictions.like("email", "%" + value + "%").ignoreCase())
                    .add(Restrictions.like("phone", "%" + value + "%").ignoreCase());

            criteria.add(completeCondition);

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Users[] user = new Users[list.size()];
                AuthUser[] aUser = new AuthUser[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    user[i] = (Users) list.get(i);
                    aUser[i] = new AuthUser();
                    aUser[i].userId = user[i].getUserid();
                    aUser[i].email = user[i].getEmail();
                    aUser[i].phoneNo = user[i].getPhone();
                    aUser[i].userName = user[i].getUsername();
                    aUser[i].statePassword = user[i].getStatus();
                    aUser[i].lLastAccessOn = user[i].getLastlogindate().getTime();
                    aUser[i].iAttempts = user[i].getAttempts();
                    aUser[i].lCreatedOn = user[i].getCreatedon().getTime();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].passwordupdatedOn = user[i].getPasswordupdatedOn();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].country = user[i].getCountry();
                    aUser[i].street = user[i].getStreet();
                    aUser[i].location = user[i].getLocation();
                    aUser[i].idType = user[i].getIdType();
                    aUser[i].userIdentity = user[i].getUserIdentity();
                    aUser[i].organisationUnit = user[i].getOrganisationUnit();
                    aUser[i].organisation = user[i].getOrganisation();
                    aUser[i].designation = user[i].getDesignation();
                }
                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }

    @Override
    public int ValidateUserDetailsByQandA(String userid, QuestionAndAnswer[] QandAObj) {
        int i = 0;

        String motherName = "julie";

        String creditLimit = "5000";
        String phoneNo = "1234567890";
        if (QandAObj == null) {
            return -1;
        }
        try {

            if (QandAObj[0].question.equals("What is your credit limit?")) {

                if (QandAObj[0].answerbyUser.equals(creditLimit)) {
                    i = i + 1;

                }

            } else if (QandAObj[0].question.equals("What is your mather's name?")) {
                if (QandAObj[0].answerbyUser.equals(motherName)) {
                    i = i + 1;

                }
            } else if (QandAObj[0].question.equals("What is your phone number?")) {
                if (QandAObj[0].answerbyUser.equals(phoneNo)) {
                    i = i + 1;
                }
            }

            if (QandAObj[1].question.equals("What is your phone number?")) {

                if (QandAObj[1].answerbyUser.equals(phoneNo)) {
                    i = i + 1;
                }

            } else if (QandAObj[1].question.equals("What is your mather's name?")) {
                if (QandAObj[1].answerbyUser.equals(motherName)) {
                    i = i + 1;
                }

            } else if (QandAObj[1].question.equals("What is your credit limit?")) {
                if (QandAObj[1].answerbyUser.equals(creditLimit)) {
                    i = i + 1;
                }

            }

            if (QandAObj[2].question.equals("What is your phone number?")) {

                if (QandAObj[2].answerbyUser.equals(phoneNo)) {
                    i = i + 1;
                }

            } else if (QandAObj[2].question.equals("What is your mather's name?")) {
                if (QandAObj[2].answerbyUser.equals(motherName)) {
                    i = i + 1;
                }

            } else if (QandAObj[2].question.equals("What is your credit limit?")) {
                if (QandAObj[2].answerbyUser.equals(creditLimit)) {
                    i = i + 1;
                }

            }

            return i;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;

    }

    @Override
    public QuestionAndAnswer[] GetValidationQuestionsAndAnswers(String userid) {
        try {

            QuestionAndAnswer[] que = new QuestionAndAnswer[3];
            que[0] = new QuestionAndAnswer();
            que[0].bEnforceCaseSensitive = true;
            que[0].bValidateAtAxiom = false;
            que[0].question = "What is your mather's name?";
            que[1] = new QuestionAndAnswer();
            que[1].bEnforceCaseSensitive = true;
            que[1].bValidateAtAxiom = false;
            que[1].question = "What is your credit limit?";
            que[2] = new QuestionAndAnswer();
            que[2].bEnforceCaseSensitive = true;
            que[2].bValidateAtAxiom = false;
            que[2].question = "What is your phone number?";
            return que;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int CreateUser(String userName, String phone, String email, String userid, int groupid, String organisation, String userIdentity, String idType, String organisationUnit, String country, String location, String street, String designation) {

        if (userid == null) {
            Date d = new Date();
            userid = HASH.getStringFromSHA1(userName + config.getChannelId() + email + phone + d.toString());
        }
        Users rUser = new Users();
        if (email == null) {
            email = "NA";
        }
        rUser.setEmail(email);
        rUser.setUserid(userid);
        rUser.setUsername(userName);
        rUser.setLastlogindate(new Date());
        rUser.setCreatedon(new Date());
        rUser.setAttempts(0);
        rUser.setPhone(phone);
        rUser.setStatus(1);
        rUser.setChannelid(config.getChannelId());
        rUser.setGroupid(groupid);        
        UUID uuid = UUID.randomUUID();
        String hashedID = HASH.getStringFromSHA1("" + new Date().getTime() + Thread.currentThread().getId() + uuid.toString());
        rUser.setSrno(hashedID);

        if (organisation == null || organisation.isEmpty()) {
            organisation = "AcledaBank PLC";
        }
        rUser.setOrganisation(organisation);
        if (country == null || country.isEmpty()) {
            country = "Cambodiya";
        }
        rUser.setCountry(country);
        if (organisationUnit == null || organisationUnit.equals("")) {
            organisationUnit = "Banking";
        }
        rUser.setOrganisationUnit(organisationUnit);
        if (userIdentity == null) {
            if (userid != null) {
                userIdentity = userid;
            }
        }
        rUser.setUserIdentity(userIdentity);
        if (idType != null) {
            rUser.setIdType(idType);
        } else {
            rUser.setIdType("Passport");
        }
        if (location == null || location.equals("")) {
            location = "Cambodiya";
        }
        if (street == null || street.equals("")) {
            street = "Street7";
        }

        rUser.setLocation(location);

        rUser.setStreet(street);
        if (designation == null || designation.equals("")) {
            designation = "Business Analyst";
        }
        rUser.setDesignation(designation);

//        rUser.setIdType(idType);
        //rUser.setPassword(null);
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        Transaction tx = null;
        try {
            tx = sSession.beginTransaction();
            sSession.save(rUser);
            tx.commit();

            return 0;
        } catch (Exception e) {

            e.printStackTrace();

        } finally {
            //sSession.close();
            //suSession.close();
            sSession.close();
            suSession.close();
        }
        return -2;
    }
    
    @Override
    public int CreateUserV2(String userName, String phone, String email, String userid, int groupid, String organisation, String userIdentity, String idType, String organisationUnit, String country, String location, String street, String designation, String tokenSrno, Integer defaultTokentype) {

        if (userid == null) {
            Date d = new Date();
            userid = HASH.getStringFromSHA1(userName + config.getChannelId() + email + phone + d.toString());
        }
        Users rUser = new Users();
        if (email == null) {
            email = "NA";
        }
        rUser.setEmail(email);
        rUser.setUserid(userid);
        rUser.setUsername(userName);
        rUser.setLastlogindate(new Date());
        rUser.setCreatedon(new Date());
        rUser.setAttempts(0);
        rUser.setPhone(phone);
        rUser.setStatus(1);
        rUser.setChannelid(config.getChannelId());
        rUser.setGroupid(groupid); 
        UUID uuid = UUID.randomUUID();
        String hashedID = HASH.getStringFromSHA1("" + new Date().getTime() + Thread.currentThread().getId() + uuid.toString());
        rUser.setSrno(hashedID);
        if (organisation == null || organisation.isEmpty()) {
            organisation = "AcledaBank PLC";
        }
        rUser.setOrganisation(organisation);
        if (country == null || country.isEmpty()) {
            country = "Cambodiya";
        }
        rUser.setCountry(country);
        if (organisationUnit == null || organisationUnit.equals("")) {
            organisationUnit = "Banking";
        }
        rUser.setOrganisationUnit(organisationUnit);
        if (userIdentity == null) {
            if (userid != null) {
                userIdentity = userid;
            }
        }
        rUser.setUserIdentity(userIdentity);
        if (idType != null) {
            rUser.setIdType(idType);
        } else {
            rUser.setIdType("Passport");
        }
        if (location == null || location.equals("")) {
            location = "Cambodiya";
        }
        if (street == null || street.equals("")) {
            street = "Street7";
        }

        rUser.setLocation(location);

        rUser.setStreet(street);
        if (designation == null || designation.equals("")) {
            designation = "Business Analyst";
        }
        rUser.setDesignation(designation);

//        rUser.setIdType(idType);
        //rUser.setPassword(null);
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        Transaction tx = null;
        try {
            tx = sSession.beginTransaction();
            sSession.save(rUser);
            tx.commit();

            return 0;
        } catch (Exception e) {

            e.printStackTrace();

        } finally {
            //sSession.close();
            //suSession.close();
            sSession.close();
            suSession.close();
        }
        return -2;
    }
    

//    @Override
//    public int getCountOfLicenseUser(int istatus) {
//        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
//        Session sSession = suSession.openSession();
//        try {
//
//            Criteria criteria = sSession.createCriteria(Users.class);
//            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
//            criteria.add(Restrictions.ne("status", istatus));
//            //criteria.add(Restrictions.eq("name", istatus));
//
//            List list = criteria.list();
//            if (list == null || list.isEmpty() == true) {
//
//                return 0;
//
//            } else {
//                int count = list.size();
//
//                return count;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            sSession.close();
//            suSession.close();
//        }
//        return 0;
//    }
    @Override
    public int getCountOfLicenseUser(int istatus) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            criteria.add(Restrictions.ne("status", istatus));
            criteria.setProjection(Projections.rowCount());
            Long count = (Long) criteria.uniqueResult();
            Integer i = (int) (long) count;
            return i;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return 0;
    }

    

    public int ChangeUserid(String oldUserid, String newUserid) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();

        try {

            Users aObj = new Users();
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", oldUserid).ignoreCase());

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return -2;
            } else {
                Date d = new Date();
                Users newObj = new Users();

                aObj = (Users) list.get(0);
                newObj.setUsername(aObj.getUsername());
                newObj.setAttempts(aObj.getAttempts());
                newObj.setChannelid(aObj.getChannelid());
                newObj.setCreatedon(aObj.getCreatedon());
                newObj.setEmail(aObj.getEmail());
                newObj.setLastlogindate(aObj.getLastlogindate());
                newObj.setPassword(aObj.getPassword());
                newObj.setPasswordupdatedOn(aObj.getPasswordupdatedOn());
                newObj.setPhone(aObj.getPhone());
                newObj.setStatus(aObj.getStatus());
                newObj.setUserid(newUserid);
                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.delete(aObj);
                    sSession.save(newObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return -5;

    }

    @Override
    public AuthUser[] getAllUserByGroup(int groupid) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        Criteria criteria = sSession.createCriteria(Users.class);
        criteria.add(Restrictions.eq("channelid", config.getChannelId()));
        try {

            criteria.add(Restrictions.eq("groupid", groupid));
            criteria.add(Restrictions.ne("status", -99));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Users[] user = new Users[list.size()];
                AuthUser[] aUser = new AuthUser[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    user[i] = (Users) list.get(i);
                    aUser[i] = new AuthUser();
                    aUser[i].userId = user[i].getUserid();
                    aUser[i].email = user[i].getEmail();
                    aUser[i].phoneNo = user[i].getPhone();
                    aUser[i].userName = user[i].getUsername();
                    aUser[i].statePassword = user[i].getStatus();
                    aUser[i].lLastAccessOn = user[i].getLastlogindate().getTime();
                    aUser[i].iAttempts = user[i].getAttempts();
                    aUser[i].lCreatedOn = user[i].getCreatedon().getTime();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].passwordupdatedOn = user[i].getPasswordupdatedOn();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].country = user[i].getCountry();
                    aUser[i].street = user[i].getStreet();
                    aUser[i].location = user[i].getLocation();
                    aUser[i].idType = user[i].getIdType();
                    aUser[i].userIdentity = user[i].getUserIdentity();
                    aUser[i].organisationUnit = user[i].getOrganisationUnit();
                    aUser[i].organisation = user[i].getOrganisation();
                    aUser[i].designation = user[i].getDesignation();

                }
                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }

    public int EditUserForSSO(AuthUser user) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();

        try {

            Users aObj = new Users();
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", user.getUserId()));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return -2;
            } else {
                Date d = new Date();

                aObj = (Users) list.get(0);
                aObj.setSsoMap(user.getSso());
                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.save(aObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return -5;

    }

    public AuthUser[] getAllUserByOrganization(String organization) {

        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("channelid", config.getChannelId()));
            criteria.add(Restrictions.eq("organisation", organization).ignoreCase());
            criteria.addOrder(Order.desc("createdon"));
            //    criteria.add(Restrictions.eq("status", istatus));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Users[] user = new Users[list.size()];
                AuthUser[] aUser = new AuthUser[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    user[i] = (Users) list.get(i);
                    aUser[i] = new AuthUser();
                    aUser[i].userId = user[i].getUserid();
                    aUser[i].email = user[i].getEmail();
                    aUser[i].phoneNo = user[i].getPhone();
                    aUser[i].userName = user[i].getUsername();
                    aUser[i].statePassword = user[i].getStatus();
                    aUser[i].status = user[i].getStatus();
                    aUser[i].lLastAccessOn = user[i].getLastlogindate().getTime();
                    aUser[i].iAttempts = user[i].getAttempts();
                    aUser[i].lCreatedOn = user[i].getCreatedon().getTime();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].designation = user[i].getDesignation();
                    aUser[i].organisation = user[i].getOrganisation();
                    aUser[i].organisationUnit = user[i].getOrganisationUnit();
                }
                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }

    public AuthUser getUserByemailorphone(String keyword) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.disjunction()
                    .add(Restrictions.eq("email", keyword).ignoreCase())
                    .add(Restrictions.eq("phone", keyword).ignoreCase()));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Users user = (Users) list.get(0);
                AuthUser aUser = new AuthUser();
                //user[i] = (Users) list.get(i);
                aUser.userId = user.getUserid();
                aUser.email = user.getEmail();
                aUser.phoneNo = user.getPhone();
                aUser.userName = user.getUsername();
                aUser.statePassword = user.getStatus();
                aUser.lLastAccessOn = user.getLastlogindate().getTime();
                aUser.iAttempts = user.getAttempts();
                aUser.lCreatedOn = user.getCreatedon().getTime();
                aUser.groupid = user.getGroupid();
                aUser.passwordupdatedOn = user.getPasswordupdatedOn();
                aUser.groupid = user.getGroupid();
                aUser.country = user.getCountry();
                aUser.street = user.getStreet();
                aUser.location = user.getLocation();
                aUser.idType = user.getIdType();
                aUser.userIdentity = user.getUserIdentity();
                aUser.organisationUnit = user.getOrganisationUnit();
                aUser.organisation = user.getOrganisation();
                aUser.designation = user.getDesignation();

                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            sSession.close();
            suSession.close();
        }

        //return null;
    }

    public int EditUserDetails(String userid, String username, String organisation, String organisationUnit, String country, String location, String street, String designation) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        try {
            Users aObj = new Users();
            Criteria criteria = sSession.createCriteria(Users.class);
            criteria.add(Restrictions.eq("userid", userid));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                //System.out.println("in failure");
                return -2;
            } else {

                aObj = (Users) list.get(0);
                aObj.setUsername(username);
                aObj.setEmail(aObj.getEmail());
                aObj.setPhone(aObj.getPhone());
                if (country != null) {
                    aObj.setCountry(country);
                }
                if (designation != null) {
                    aObj.setDesignation(designation);
                }
                if (location != null) {
                    aObj.setLocation(location);
                }
                if (street != null) {
                    aObj.setStreet(street);
                }
                if (organisation != null) {
                    aObj.setOrganisation(organisation);
                }
                if (organisationUnit != null) {
                    aObj.setOrganisationUnit(organisationUnit);
                }

                Transaction tx = null;
                try {
                    tx = sSession.beginTransaction();
                    sSession.save(aObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return -5;

    }

    @Override
    public AuthUser[] SearchUserByStatusBetDate(String value, int istatus, Date fromDate, Date endDate) {

        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.users);
        Session sSession = suSession.openSession();
        Criteria criteria = sSession.createCriteria(Users.class);
        criteria.add(Restrictions.eq("channelid", config.getChannelId()));
        try {
            if (istatus == 99) { //this meaning listing for all
            } else { //this meanings listing for specific 
                criteria.add(Restrictions.eq("status", istatus));
            }

            Criterion completeCondition
                    = Restrictions.disjunction()
                    .add(Restrictions.like("username", "%" + value + "%").ignoreCase())
                    .add(Restrictions.like("email", "%" + value + "%").ignoreCase())
                    .add(Restrictions.like("phone", "%" + value + "%").ignoreCase());

            criteria.add(completeCondition);
            if (fromDate != null && endDate != null) {
                fromDate.setHours(0);
                fromDate.setMinutes(0);
                fromDate.setSeconds(0);

                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                criteria.add(Restrictions.ge("createdon", fromDate));
                criteria.add(Restrictions.le("createdon", endDate));
            }

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Users[] user = new Users[list.size()];
                AuthUser[] aUser = new AuthUser[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    user[i] = (Users) list.get(i);
                    aUser[i] = new AuthUser();
                    aUser[i].userId = user[i].getUserid();
                    aUser[i].email = user[i].getEmail();
                    aUser[i].phoneNo = user[i].getPhone();
                    aUser[i].userName = user[i].getUsername();
                    aUser[i].statePassword = user[i].getStatus();
                    aUser[i].lLastAccessOn = user[i].getLastlogindate().getTime();
                    aUser[i].iAttempts = user[i].getAttempts();
                    aUser[i].lCreatedOn = user[i].getCreatedon().getTime();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].passwordupdatedOn = user[i].getPasswordupdatedOn();
                    aUser[i].groupid = user[i].getGroupid();
                    aUser[i].country = user[i].getCountry();
                    aUser[i].street = user[i].getStreet();
                    aUser[i].location = user[i].getLocation();
                    aUser[i].idType = user[i].getIdType();
                    aUser[i].userIdentity = user[i].getUserIdentity();
                    aUser[i].organisationUnit = user[i].getOrganisationUnit();
                    aUser[i].organisation = user[i].getOrganisation();
                    aUser[i].designation = user[i].getDesignation();
                }
                return aUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }

}
