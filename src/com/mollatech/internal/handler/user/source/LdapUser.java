package com.mollatech.internal.handler.user.source;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.Answer;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.AxiomConfigProvider;
import com.mollatech.axiom.connector.user.AxiomExternalSourceInterface;
import com.mollatech.axiom.connector.user.OrganizationDetails;
import com.mollatech.axiom.connector.user.PasswordPolicyDetails;
import com.mollatech.axiom.connector.user.Question;
import com.mollatech.axiom.connector.user.QuestionAndAnswer;
import com.mollatech.axiom.connector.user.SourceDetails;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class LdapUser implements AxiomExternalSourceInterface
{
  public static final int ACTIVE = 1;
  public static final int BLOCKED = -1;
  public static final int DELETED = -99;
  public static String AES = "AES_ENCRYPTION";
  public static String SHA256 = "SHA256_HASH";
  private Connection cn;
  private PreparedStatement pst;
  private PreparedStatement pst1;
  private ResultSet rs;
  public static AxiomConfigProvider config = null;
  
  public AuthUser getUser(String userid)
  {
    Hashtable env = new Hashtable();
    String provider_url = "ldap://".concat(config.getSourceDetails().getHostname()).concat(":").concat(Integer.toString(config.getSourceDetails().getPortno()));
    env.put("java.naming.security.authentication", "simple");
    if (config.getSourceDetails().getUserName() != null) {
      env.put("java.naming.security.principal", config.getSourceDetails().getUserName());
    }
    if (config.getSourceDetails().getPassword() != null) {
      env.put("java.naming.security.credentials", config.getSourceDetails().getPassword());
    }
    env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
    env.put("java.naming.provider.url", provider_url);
    AuthUser aUser = null;
    DirContext ctx;
    try
    {
      ctx = new InitialDirContext(env);
    }
    catch (NamingException e)
    {
      throw new RuntimeException(e);
    }
    NamingEnumeration results = null;
    try
    {
      SearchControls controls = new SearchControls();
      controls.setSearchScope(2);
      results = ctx.search(config.getSourceDetails().getDatabaseName(), "(userid=" + userid + ")", controls);
      SearchResult searchResult = (SearchResult)results.next();
      Attributes attributes = searchResult.getAttributes();
      aUser = new AuthUser();
      aUser.userId = userid;
      aUser.email = ((String)attributes.get("mail").get());
      aUser.phoneNo = ((String)attributes.get("mobile").get());
      aUser.userName = String.valueOf(attributes.get("cn").get()).concat(" ").concat(String.valueOf(attributes.get("sn").get()));
      
      return aUser;
    }
    catch (NullPointerException e) {}catch (NamingException ex)
    {
      Logger.getLogger(LdapUser.class.getName()).log(Level.SEVERE, null, ex);
    }
    finally
    {
      if (results != null) {
        try
        {
          results.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
      if (ctx != null) {
        try
        {
          ctx.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
    }
    return null;
  }
  
  public AuthUser[] SearchUsers(String value)
  {
    Hashtable env = new Hashtable();
    String provider_url = "ldap://".concat(config.getSourceDetails().getHostname()).concat(":").concat(Integer.toString(config.getSourceDetails().getPortno()));
    env.put("java.naming.security.authentication", "simple");
    if (config.getSourceDetails().getUserName() != null) {
      env.put("java.naming.security.principal", config.getSourceDetails().getUserName());
    }
    if (config.getSourceDetails().getPassword() != null) {
      env.put("java.naming.security.credentials", config.getSourceDetails().getPassword());
    }
    env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
    
    env.put("java.naming.provider.url", provider_url);
    AuthUser[] aUser = new AuthUser[1];
    DirContext ctx;
    try
    {
      ctx = new InitialDirContext(env);
    }
    catch (NamingException e)
    {
      throw new RuntimeException(e);
    }
    NamingEnumeration results = null;
    try
    {
      SearchControls controls = new SearchControls();
      controls.setSearchScope(2);
      results = ctx.search(config.getSourceDetails().getDatabaseName(), "(" + config.getSourceDetails().getTableName() + "=" + value + ")", controls);
      SearchResult searchResult = (SearchResult)results.next();
      Attributes attributes = searchResult.getAttributes();
      aUser[0] = new AuthUser();
      aUser[0].userId = value;
      aUser[0].email = ((String)attributes.get("mail").get());
      aUser[0].phoneNo = ((String)attributes.get("mobile").get());
      aUser[0].userName = String.valueOf(attributes.get("cn").get()).concat(" ").concat(String.valueOf(attributes.get("sn").get()));
      
      return aUser;
    }
    catch (NullPointerException e) {}catch (NamingException ex)
    {
      Logger.getLogger(LdapUser.class.getName()).log(Level.SEVERE, null, ex);
    }
    finally
    {
      if (results != null) {
        try
        {
          results.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
      if (ctx != null) {
        try
        {
          ctx.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
    }
    return null;
  }
  
  public AuthUser[] getAllUsers()
  {
    Hashtable env = new Hashtable();
    String provider_url = "ldap://".concat(config.getSourceDetails().getHostname()).concat(":").concat(Integer.toString(config.getSourceDetails().getPortno()));
    env.put("java.naming.security.authentication", "simple");
    if (config.getSourceDetails().getUserName() != null) {
      env.put("java.naming.security.principal", config.getSourceDetails().getUserName());
    }
    if (config.getSourceDetails().getPassword() != null) {
      env.put("java.naming.security.credentials", config.getSourceDetails().getPassword());
    }
    env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
    env.put("java.naming.provider.url", provider_url);
    int sizecount = 0;
    DirContext ctx;
    try
    {
      ctx = new InitialDirContext(env);
    }
    catch (NamingException e)
    {
      throw new RuntimeException(e);
    }
    NamingEnumeration results = null;
    try
    {
      SearchControls controls = new SearchControls();
      controls.setSearchScope(2);
      
      results = ctx.search(config.getSourceDetails().getDatabaseName(), "(objectclass=*)", controls);
      while (results.hasMore())
      {
        SearchResult searchResult = (SearchResult)results.next();
        Attributes attributes = searchResult.getAttributes();
        if (attributes.size() != 2) {
          sizecount++;
        }
      }
      AuthUser[] aUser = new AuthUser[sizecount - 1];
      Attributes attributes;
      while (results.hasMore())
      {
        sizecount = 0;
        SearchResult searchResult = (SearchResult)results.next();
        attributes = searchResult.getAttributes();
        if (attributes.size() != 2)
        {
          aUser[sizecount] = new AuthUser();
          aUser[sizecount].userId = ((String)attributes.get("userid").get());
          aUser[sizecount].email = ((String)attributes.get("mail").get());
          aUser[sizecount].phoneNo = ((String)attributes.get("mobile").get());
          aUser[sizecount].userName = String.valueOf(attributes.get("cn").get()).concat(" ").concat(String.valueOf(attributes.get("sn").get()));
          sizecount++;
        }
      }
      return aUser;
    }
    catch (NameNotFoundException e)
    {
      System.out.println("Error : " + e);
    }
    catch (NamingException e)
    {
      throw new RuntimeException(e);
    }
    finally
    {
      if (results != null) {
        try
        {
          results.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
      if (ctx != null) {
        try
        {
          ctx.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
    }
    return null;
  }
  
  public AXIOMStatus Load(AxiomConfigProvider axiomConfigProvider)
  {
    AXIOMStatus ax = new AXIOMStatus();
    try
    {
      config = axiomConfigProvider;
      Class.forName(config.getSourceDetails().getDatabaseType());
      ax.iStatus = 0;
      ax.strStatus = "success";
      return ax;
    }
    catch (Exception ex)
    {
      ax.strStatus = ex.getMessage();
      
      ax.iStatus = -1;
    }
    return ax;
  }
  
  public AXIOMStatus Unload()
  {
    AXIOMStatus ax = new AXIOMStatus();
    ax.iStatus = 0;
    ax.strStatus = "success";
    return ax;
  }
  
  public int getCountOfUsers()
  {
    Hashtable env = new Hashtable();
    String provider_url = "ldap://".concat(config.getSourceDetails().getHostname()).concat(":").concat(Integer.toString(config.getSourceDetails().getPortno()));
    env.put("java.naming.security.authentication", "simple");
    if (config.getSourceDetails().getUserName() != null) {
      env.put("java.naming.security.principal", config.getSourceDetails().getUserName());
    }
    if (config.getSourceDetails().getPassword() != null) {
      env.put("java.naming.security.credentials", config.getSourceDetails().getPassword());
    }
    env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
    env.put("java.naming.provider.url", provider_url);
    int sizecount = 0;
    DirContext ctx;
    try
    {
      ctx = new InitialDirContext(env);
    }
    catch (NamingException e)
    {
      throw new RuntimeException(e);
    }
    NamingEnumeration results = null;
    try
    {
      SearchControls controls = new SearchControls();
      controls.setSearchScope(2);
      results = ctx.search(config.getSourceDetails().getDatabaseName(), "(objectclass=*)", controls);
      SearchResult searchResult;
      while (results.hasMore())
      {
        searchResult = (SearchResult)results.next();
        Attributes attributes = searchResult.getAttributes();
        if (attributes.size() != 2) {
          sizecount++;
        }
      }
      return sizecount;
    }
    catch (NameNotFoundException e)
    {
      System.out.println("Error : " + e);
    }
    catch (NamingException e)
    {
      throw new RuntimeException(e);
    }
    finally
    {
      if (results != null) {
        try
        {
          results.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
      if (ctx != null) {
        try
        {
          ctx.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
    }
    return 0;
  }
  
  public int CreateUser(String username, String phone, String email, String userid)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  
  public int CreateUserV2(String userName, String phone, String email, String userid,int groupid,String organisation,String userIdentity,String idType,String organisationUnit,String country,String location,String street, String designation, String tokenSrno, Integer default_token)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int AssignPassword(String userid, String password, PasswordPolicyDetails passwordpolicy)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int ChangeStatus(String userid, int iType)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int EditUser(String userid, String username, String phone, String email)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public AuthUser CheckUserUsingPhone(String phoneNumber)
  {
    Hashtable env = new Hashtable();
    String provider_url = "ldap://".concat(config.getSourceDetails().getHostname()).concat(":").concat(Integer.toString(config.getSourceDetails().getPortno()));
    env.put("java.naming.security.authentication", "simple");
    if (config.getSourceDetails().getUserName() != null) {
      env.put("java.naming.security.principal", config.getSourceDetails().getUserName());
    }
    if (config.getSourceDetails().getPassword() != null) {
      env.put("java.naming.security.credentials", config.getSourceDetails().getPassword());
    }
    env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
    env.put("java.naming.provider.url", provider_url);
    AuthUser aUser = null;
    DirContext ctx;
    try
    {
      ctx = new InitialDirContext(env);
    }
    catch (NamingException e)
    {
      throw new RuntimeException(e);
    }
    NamingEnumeration results = null;
    try
    {
      SearchControls controls = new SearchControls();
      controls.setSearchScope(2);
      results = ctx.search(config.getSourceDetails().getDatabaseName(), "(mobile=" + phoneNumber + ")", controls);
      SearchResult searchResult = (SearchResult)results.next();
      Attributes attributes = searchResult.getAttributes();
      aUser = new AuthUser();
      aUser.userId = ((String)attributes.get("userid").get());
      aUser.email = ((String)attributes.get("mail").get());
      aUser.phoneNo = ((String)attributes.get("mobile").get());
      aUser.userName = String.valueOf(attributes.get("cn").get()).concat(" ").concat(String.valueOf(attributes.get("sn").get()));
      
      return aUser;
    }
    catch (NullPointerException e) {}catch (NamingException ex)
    {
      Logger.getLogger(LdapUser.class.getName()).log(Level.SEVERE, null, ex);
    }
    finally
    {
      if (results != null) {
        try
        {
          results.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
      if (ctx != null) {
        try
        {
          ctx.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
    }
    return null;
  }
  
  public AuthUser CheckUserUsingEmail(String emailid)
  {
    Hashtable env = new Hashtable();
    String provider_url = "ldap://".concat(config.getSourceDetails().getHostname()).concat(":").concat(Integer.toString(config.getSourceDetails().getPortno()));
    env.put("java.naming.security.authentication", "simple");
    if (config.getSourceDetails().getUserName() != null) {
      env.put("java.naming.security.principal", config.getSourceDetails().getUserName());
    }
    if (config.getSourceDetails().getPassword() != null) {
      env.put("java.naming.security.credentials", config.getSourceDetails().getPassword());
    }
    env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
    env.put("java.naming.provider.url", provider_url);
    AuthUser aUser = null;
    DirContext ctx;
    try
    {
      ctx = new InitialDirContext(env);
    }
    catch (NamingException e)
    {
      throw new RuntimeException(e);
    }
    NamingEnumeration results = null;
    try
    {
      SearchControls controls = new SearchControls();
      controls.setSearchScope(2);
      results = ctx.search(config.getSourceDetails().getDatabaseName(), "(mail=" + emailid + ")", controls);
      SearchResult searchResult = (SearchResult)results.next();
      Attributes attributes = searchResult.getAttributes();
      aUser = new AuthUser();
      aUser.userId = ((String)attributes.get("userid").get());
      aUser.email = ((String)attributes.get("mail").get());
      aUser.phoneNo = ((String)attributes.get("mobile").get());
      aUser.userName = String.valueOf(attributes.get("cn").get()).concat(" ").concat(String.valueOf(attributes.get("sn").get()));
      return aUser;
    }
    catch (NullPointerException e) {}catch (NamingException ex)
    {
      Logger.getLogger(LdapUser.class.getName()).log(Level.SEVERE, null, ex);
    }
    finally
    {
      if (results != null) {
        try
        {
          results.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
      if (ctx != null) {
        try
        {
          ctx.close();
        }
        catch (Exception e)
        {
          System.out.println("Error : " + e);
        }
      }
    }
    return null;
  }
  
  public boolean TestConnection()
  {
    Hashtable env = new Hashtable();
    String provider_url = "ldap://".concat(config.getSourceDetails().getHostname()).concat(":").concat(Integer.toString(config.getSourceDetails().getPortno()));
    env.put("java.naming.security.authentication", "simple");
    if (config.getSourceDetails().getUserName() != null) {
      env.put("java.naming.security.principal", config.getSourceDetails().getUserName());
    }
    if (config.getSourceDetails().getPassword() != null) {
      env.put("java.naming.security.credentials", config.getSourceDetails().getPassword());
    }
    env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
    env.put("java.naming.provider.url", provider_url);
    AuthUser aUser = null;
    try
    {
      DirContext ctx = new InitialDirContext(env);
      return true;
    }
    catch (NamingException e)
    {
      throw new RuntimeException(e);
    }
  }
  
  public OrganizationDetails GetOrganizationDetails(String userid)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public AuthUser[] getAllUserByStatus(int limit)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public AuthUser[] SearchUsersByStatus(String value, int istatus)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int getCountOfUserByStatus(int istatus, String keyword)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public AuthUser[] SearchUsersByID(String UserID)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public QuestionAndAnswer[] GetValidationQuestionsAndAnswers(String userid)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int ValidateUserDetailsByQandA(String userid, QuestionAndAnswer[] QandAObj)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int getCountOfLicenseUser(int istatus)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public AuthUser CheckUserUsingName(String username)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public AuthUser verifyPassword(String usereid, String password)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int changePassword(String userid, String oldpassword, String newpassword)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int resetPassword(String userid)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public Question[] GetValidationQuestions(String userid)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int ValidateUserDetails(String userid, Answer[] ans)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public String GetPassword(String userid)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int CreateUser(String userName, String phone, String email, String userid, int groupid, String organisation, String userIdentity, String idType, String organisationUnit, String country, String location, String street, String designation)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int EditUser(String userid, String username, String phone, String email, int groupid, String organisation, String userIdentity, String idType, String organisationUnit, String country, String location, String street, String designation)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public int EditUserForPush(String userid, String username, String phone, String email, int groupid, String organisation, String userIdentity, String idType, String organisationUnit, String country, String location, String street, String designation, Integer defaultTokenType)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public AuthUser[] getAllUserByGroup(int groupid)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public AuthUser[] SearchUserByStatusBetDate(String value, int istatus, Date fromDate, Date endDate)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

    @Override
    public int removePassword(String userid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
