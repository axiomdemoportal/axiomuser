package com.mollatech.internal.handler.user.source;

import java.util.Iterator;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
public class SessionFactoryUtil extends SessionFactory {

    private static org.hibernate.SessionFactory sessionFactory;
     private org.hibernate.Session m_session = null;
        public static final int users = 1;

    public SessionFactoryUtil(int iType) {
       
        try {
            if (sessionFactory != null) {
                return;
            }
            AnnotationConfiguration annotationConfig = new AnnotationConfiguration();
            Set set = stat_Properties.keySet();
            Iterator itr = set.iterator();
            while (itr.hasNext()) {
                String key = (String) itr.next();
                String value = (String) stat_Properties.get(key);
                annotationConfig.setProperty(key, value);
            }

            if (iType == users) {
                annotationConfig.addResource("com/mollatech/internal/handler/user/source/Users.hbm.xml");
                annotationConfig.addPackage("com/mollatech/internal/handler/user/source/");
                annotationConfig.addAnnotatedClass(Users.class);
            }

            sessionFactory = annotationConfig.buildSessionFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Session openSession() {
        m_session =sessionFactory.openSession();
        return m_session;
        
    }

    public void close() {
//        if (sessionFactory != null) {
//            sessionFactory.close();
//        }
//        sessionFactory = null;
    }
}
