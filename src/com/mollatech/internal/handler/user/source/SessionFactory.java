package com.mollatech.internal.handler.user.source;

import com.mollatech.axiom.connector.user.SourceDetails;
import java.util.Properties;

public class SessionFactory {

    public static Properties stat_Properties = null;

    public SessionFactory() {

        try {
       
                SourceDetails srcDetailsObj = AxiomUser.config.getSourceDetails();
                if (srcDetailsObj.getDbCategory().equalsIgnoreCase("Mysql")) {    // new change by abhishek
                    Properties p = new Properties();
                    p.put("hibernate.connection.url", "jdbc:mysql://"
                            + srcDetailsObj.getHostname() + ":"
                            + srcDetailsObj.getPortno() + "/"
                            + srcDetailsObj.getDatabaseName()
                            + "?autoReconnect=true");
                    p.put("hibernate.connection.username",
                            srcDetailsObj.getUserName());
                    String passwd = srcDetailsObj.getPassword();
                    p.put("hibernate.connection.password", passwd);
                    p.put("hibernate.connection.driver_class", srcDetailsObj.getDatabaseType());
                    p.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
                    p.put("hibernate.transaction.factory_class", "org.hibernate.transaction.JDBCTransactionFactory");
                    p.put("hibernate.current_session_context_class", "thread");
                    p.put("hibernate.show_sql", "false");
                    p.put("log4j.logger.org.hibernate", "error");
                    stat_Properties = p;
                } else if (srcDetailsObj.getDbCategory().equalsIgnoreCase("Oracle")) { // new change by abhishek
                    Properties p = new Properties();

//                    p.put("hibernate.connection.url", "jdbc:oracle:thin:@"
//                            + srcDetailsObj.getHostname() + ":"
//                            + srcDetailsObj.getPortno() + ":"
//                            + srcDetailsObj.getDatabaseName()
                          //Changes for Oracle Service Name
                       p.put("hibernate.connection.url", "jdbc:oracle:thin:@"
                            + srcDetailsObj.getHostname() + ":"
                            + srcDetailsObj.getPortno() + "/"
                            + srcDetailsObj.getDatabaseName()      
                            
                    );
                    p.put("hibernate.connection.username",
                            srcDetailsObj.getUserName());

                    String passwd = srcDetailsObj.getPassword();
                    p.put("hibernate.connection.password", passwd);
                    p.put("hibernate.connection.driver_class", srcDetailsObj.getDatabaseType());
                    p.put("hibernate.dialect", "org.hibernate.dialect.OracleDialect");
                    p.put("hibernate.transaction.factory_class", "org.hibernate.transaction.JDBCTransactionFactory");
                    p.put("hibernate.current_session_context_class", "thread");
                    p.put("hibernate.show_sql", "false");
                    p.put("log4j.logger.org.hibernate", "error");
                    stat_Properties = p;
                }
            

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
