//package com.mollatech.internal.handler.user.source;
//import java.util.Date;
//
//public class Users  implements java.io.Serializable {
//
//
//     private String userid;
//     private String channelid;
//     private String username;
//     private String password;
//     private String phone;
//     private String email;
//     private int status;
//     private int attempts;
//     private Date createdon;
//     private Date lastlogindate;
//     private Date passwordupdatedOn;
//     private int groupid;
//
//      private Date accessTill;
//     private Date accessFrom;
//
//    public Date getAccessTill() {
//        return accessTill;
//    }
//
//    public void setAccessTill(Date accessTill) {
//        this.accessTill = accessTill;
//    }
//
//    public Date getAccessFrom() {
//        return accessFrom;
//    }
//
//    public void setAccessFrom(Date accessFrom) {
//        this.accessFrom = accessFrom;
//    }
//
//     
//    public int getGroupid() {
//        return groupid;
//    }
//
//    public void setGroupid(int groupid) {
//        this.groupid = groupid;
//    }
//
//    public Users() {
//    }
//
//    public Date getPasswordupdatedOn() {
//        return passwordupdatedOn;
//    }
//
//    public void setPasswordupdatedOn(Date passwordupdatedOn) {
//        this.passwordupdatedOn = passwordupdatedOn;
//    }
//
//	
//    public Users(String userid, String channelid, String username, int status, int attempts, Date lastlogindate,int groupid) {
//        this.userid = userid;
//        this.channelid = channelid;
//        this.username = username;
//        this.status = status;
//        this.attempts = attempts;
//        this.lastlogindate = lastlogindate;
//        this.groupid = groupid;
//    }
//    public Users(String userid, String channelid, String username, String password, String phone, String email, int status, int attempts, Date createdon, Date lastlogindate,int groupid) {
//       this.userid = userid;
//       this.channelid = channelid;
//       this.username = username;
//       this.password = password;
//       this.phone = phone;
//       this.email = email;
//       this.status = status;
//       this.attempts = attempts;
//       this.createdon = createdon;
//       this.lastlogindate = lastlogindate;
//       this.groupid = groupid;
//    }
//   
//    public String getUserid() {
//        return this.userid;
//    }
//    
//    public void setUserid(String userid) {
//        this.userid = userid;
//    }
//    public String getChannelid() {
//        return this.channelid;
//    }
//    
//    public void setChannelid(String channelid) {
//        this.channelid = channelid;
//    }
//    public String getUsername() {
//        return this.username;
//    }
//    
//    public void setUsername(String username) {
//        this.username = username;
//    }
//    public String getPassword() {
//        return this.password;
//    }
//    
//    public void setPassword(String password) {
//        this.password = password;
//    }
//    public String getPhone() {
//        return this.phone;
//    }
//    
//    public void setPhone(String phone) {
//        this.phone = phone;
//    }
//    public String getEmail() {
//        return this.email;
//    }
//    
//    public void setEmail(String email) {
//        this.email = email;
//    }
//    public int getStatus() {
//        return this.status;
//    }
//    
//    public void setStatus(int status) {
//        this.status = status;
//    }
//    public int getAttempts() {
//        return this.attempts;
//    }
//    
//    public void setAttempts(int attempts) {
//        this.attempts = attempts;
//    }
//    public Date getCreatedon() {
//        return this.createdon;
//    }
//    
//    public void setCreatedon(Date createdon) {
//        this.createdon = createdon;
//    }
//    public Date getLastlogindate() {
//        return this.lastlogindate;
//    }
//    
//    public void setLastlogindate(Date lastlogindate) {
//        this.lastlogindate = lastlogindate;
//    }
//
//
//
//
//}
//
//
package com.mollatech.internal.handler.user.source;

import java.util.Date;

public class Users implements java.io.Serializable {
    private String srno;
    private String userid;
    private String channelid;
    private String username;
    private String password;
    private String phone;
    private String email;
    private int status;
    private int attempts;
    private Date createdon;
    private Date lastlogindate;
    private Date passwordupdatedOn;
    private int groupid;
    private String country;
    private String location;
    private String street;
    private String organisation;
    private String userIdentity;
    private String idType;
    private String organisationUnit;
    private String designation;
    private byte[] ssoMap;      
    
    public String getSrno() {
        return this.srno;
    }
//    
    public void setSrno(String srno) {
        this.srno = srno;
    }
    
    public String getDesignation() {
        return designation;
    }

    public byte[] getSsoMap() {
        return ssoMap;
    }

    public void setSsoMap(byte[] ssoMap) {
        this.ssoMap = ssoMap;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

//    public String getcountry() {
//        return this.country;
//    }
//
//    public String getlocation() {
//        return this.location;
//    }
//
//    public String getstreet() {
//        return this.street;
//    }
//
//    public String getorganisation() {
//        return this.organisation;
//    }
//
//    public String getuserIdentity() {
//        return this.userIdentity;
//    }
//
//    public String getidType() {
//        return this.idType;
//    }
//
//    public String getorganisationUnit() {
//        return this.organisationUnit;
//    }
//

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getUserIdentity() {
        return userIdentity;
    }

    public void setUserIdentity(String userIdentity) {
        this.userIdentity = userIdentity;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getOrganisationUnit() {
        return organisationUnit;
    }

    public void setOrganisationUnit(String organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

//    public void setorganisationUnit(String organisationUnit ) {
//        this.organisationUnit = organisationUnit;
//
//    }
//
//    public void setidType(String idType) {
//        this.idType = idType;
//
//    }
//
//    public void setuserIdentity(String userIdentity) {
//        this.userIdentity = userIdentity;
//
//    }
//
//    public void setorganisation(String organisation) {
//        
//        this.organisation = organisation;
//
//    }
//
//    public void setstreet(String street) {
//        this.street = street;
//
//    }
//
//    public void setlocation(String location) {
//        this.location = location;
//
//    }
//
//    public void setcountry(String country) {
//        this.country = country;
//
//    }
    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    public Users() {
    }

    public Date getPasswordupdatedOn() {
        return passwordupdatedOn;
    }

    public void setPasswordupdatedOn(Date passwordupdatedOn) {
        this.passwordupdatedOn = passwordupdatedOn;
    }

    public Users(String userid, String channelid, String username, int status, int attempts, Date lastlogindate, int groupid) {
        this.userid = userid;
        this.channelid = channelid;
        this.username = username;
        this.status = status;
        this.attempts = attempts;
        this.lastlogindate = lastlogindate;
        this.groupid = groupid;
    }

    public Users(String userid, String channelid, String username, String password, String phone, String email, int status, int attempts, Date createdon, Date lastlogindate, int groupid) {
        this.userid = userid;
        this.channelid = channelid;
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.email = email;
        this.status = status;
        this.attempts = attempts;
        this.createdon = createdon;
        this.lastlogindate = lastlogindate;
        this.groupid = groupid;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getChannelid() {
        return this.channelid;
    }

    public void setChannelid(String channelid) {
        this.channelid = channelid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAttempts() {
        return this.attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public Date getLastlogindate() {
        return this.lastlogindate;
    }

    public void setLastlogindate(Date lastlogindate) {
        this.lastlogindate = lastlogindate;
    }

}
